<?php
declare (strict_types=1);

namespace quick\admin\table\traits;


use quick\admin\filter\Filter;
use quick\admin\table\Query;
use quick\admin\table\Column;
use SebastianBergmann\CodeCoverage\TestFixture\C;
use think\helper\Str;
use think\paginator\driver\Bootstrap;

trait BuildTableDataTrait
{


    /**
     * @var Query
     */
    public $query;

    /**
     * @var string
     */
    public static $searchKey = '_search_';

    /**
     * @var string
     */
    public static $searchTypeKey = '_search_type_';


    /**
     *  排序字段参数key
     * @var string
     */
    public static $_sortKey = "_sort";

    /**
     *
     * @var string
     */
    public static $_orderKey = "_order";


    /**
     * 快捷搜索
     *
     * @var array|string|\Closure
     */
    protected $search;

    /**
     * @var string 搜索提示
     */
    protected string $searchPlaceholder = '';

    /**
     * @var string 搜索提示
     */
    protected string $searchTypePlaceholder = '';


    /**
     * @var Filter
     */
    protected $filter;

    /**
     * @var \Closure
     */
    protected $dataCallback;


    /**
     * 获取查询对象
     *
     * @return Query
     */
    public function getQuery()
    {
        if (!($this->query instanceof Query)) {
            $query = new Query($this->model);
            if (!$this->usePaginate) {
                $query->disablePagination();
            }
            $this->query = $query;
        }
        return $this->query;
    }


    /**
     *
     * 禁用分页
     *
     * @return $this
     */
    public function disablePagination()
    {
        $this->usePaginate = false;
        return $this;
    }


    /**
     *
     * 设置快捷搜索
     *
     * @param array $searchTypes
     * 前端选择字段类型搜索[ ['label' => '名称', 'value' => 'name'],  ['label' => '编号', 'value' => 'no']]
     * 全匹配搜索 ['id','name']
     * @param string $searchPlaceholder
     * @param string $searchTypePlaceholder
     * @return $this
     */
    public function search(array $searchTypes = [],string $searchPlaceholder = '',string $searchTypePlaceholder = '')
    {
        $this->search = $searchTypes;
        $this->searchPlaceholder = $searchPlaceholder;
        $this->searchTypePlaceholder = $searchTypePlaceholder;
        return $this;
    }


    /**
     * @return array
     */
    private function getSearch()
    {
        if (empty($this->search)) {
            return false;
        }
        $typeList = is_array(end($this->search)) ?  $this->search : [];
        return [
            'typeList' => $typeList,
            'defaultType' => '',
            'defaultValue' => '',
            'typeKey' => static::$searchTypeKey,
            'valueKey' => static::$searchKey,
            'placeholder' => __($this->searchPlaceholder),
            'selectPlaceholder' => __($this->searchTypePlaceholder),
        ];
    }


    /**
     * @param Filter $filter
     * @return $this
     */
    public function setFilter(Filter $filter)
    {
        $this->filter = $filter;
        return $this;
    }


    /**
     * @param \Closure $func
     * @return $this
     */
    public function filter(\Closure $func)
    {
        $filter = Filter::make();
        call_user_func($func, $filter);
        $this->setFilter($filter);
        return $this;
    }


    /**
     * @return Filter
     */
    public function getFilter()
    {
        return $this->filter;
    }

    /**
     * 处理快捷搜索
     *
     * @return $this
     */
    public function handleQuickSearch()
    {
        $value = request()->param(static::$searchKey,'',['strip_tags','trim']);
        $typeValue = request()->param(static::$searchTypeKey,'',['strip_tags','trim']);
        if (!$value || empty($this->search)) {
            return $this;
        }

        if (is_array(end($this->search))) {
            foreach($this->search as $item){
                if($item['value'] === $typeValue){
                    $columns = $item['value'];
                    break;
                }
            }
        } else if (is_array($this->search)) {
            $columns = implode("|", $this->search);
        } else if (is_string($this->search)) {
            $columns = $this->search;
        }

        !empty($columns) && $this->getQuery()->where($columns, "like", "%{$value}%");

        return $this;
    }


    /**
     * 处理过滤器
     *
     */
    public function handleFilter()
    {
        $filter = $this->getFilter();
        if ($filter instanceof Filter) {
            $conditions = $filter->conditions();
            $this->getQuery()->addConditions($conditions);
        }

    }

    /**
     * 处理排序
     *
     */
    public function handleSort()
    {
        $order = $this->request->param(self::$_orderKey);
        $sort = $this->request->param(self::$_sortKey);
        if (!empty($order) && !empty($sort)) {

            $this->props(['default-sort' => [
                'prop' => $sort,
                'order' => $order,
            ]]);

            $order = $order == 'descending' ? 'asc' : 'desc';
            $this->getQuery()->order($sort, $order);
        }
    }


    /**
     * 设置数据回调
     *
     * @param \Closure $dataCallback
     * @return $this
     */
    public function dataUsing(\Closure $dataCallback)
    {
        $this->dataCallback = $dataCallback;
        return $this;
    }


    /**
     * @return array|\think\Collection
     * @throws \Exception
     */
    public function handleQuery()
    {
        if (!empty($this->rows)) {
            return $this->rows;
        }
        if (empty($this->getModel())) {
            return [];
        }

        !$this->usePaginate && $this->getQuery()->disablePagination();
        $this->handleQuickSearch();
        $this->handleFilter();
        $this->handleSort();
        $data = $this->getQuery()->paginate($this->perPage)->buildData();


        if ($data instanceof Bootstrap) {
            $total = $data->total();
            $currentPage = $data->currentPage();
            $data = $data->getCollection();
        } else {

            $total = isset($data['total']) ? $data['total'] : count($data);
            $currentPage = isset($data['current_page']) ? $data['current_page'] : 1;
        }

        if ($this->usePaginate) {
            $this->setPaginate($total, $currentPage);
        }


        // 数据回调
        if ($this->dataCallback) {
            $data = call_user_func($this->dataCallback, $data);
        }
        $this->rows = $data;

        return $this->rows;
    }

    /**
     * columns 数组树展开
     *
     * @param array $columns
     * @return array
     */
    protected function flattenColumns(array $columns)
    {
        $list = [];
        foreach ($columns as $column) {
            if ($column instanceof Column) {
                if ($children = $column->getChildren()) {
                    $children = $this->flattenColumns((array)$children);
                }
                if ($children) {
                    //存在子类的column不显示数据
                    $list = array_merge($list, $children);
                } else {
                    $list[] = $column;
                }
            }

        }
        return $list;
    }

    /**
     * 构建table dataList
     *
     * @return \think\Collection
     * @throws \Exception
     */
    public function buildData()
    {
        $this->handleQuery();
        $data = $this->rows;

        $columns = array_merge($this->getVisibleColumns(false), [$this->getKeyColumn()]);
        $columns = $this->flattenColumns($columns);
        $data = collect($data)->map(function ($row) use ($columns) {
            return $this->resolveRow($row, $columns);
        });

        $paginate = $this->getPaginate();
        if ($paginate) {
            $data = $paginate->buildData($data);
        } else {
            $data = [
                'data' => $data
            ];
        }

        return $data;
    }


    /**
     * 默认显示字段
     *
     * @return array
     */
    public function visibleColumnKeys()
    {
        $columns = $this->flattenColumns($this->getVisibleColumns());
//        $columns = $this->getVisibleColumns();
        $keys = [];
        foreach ($columns as $column) {
            if (!in_array($column->name, $this->hideColumns)) {
                $keys[] = $column->name;
            }
        }

        return $keys;
    }


    /**
     * 处理row
     * @param $row
     * @param $columns
     * @return array
     */
    protected function resolveRow($row, $columns,$parentId = null)
    {

        $item = [];
        $childrenKey = 'children';//子节点key
        $children = data_get($row, str_replace('->', '.', $childrenKey));
        $childKey = '_children';
        if($this->virtualize){
            $childKey = 'children';
        }
        if (is_array($children)) {
            foreach ($children as $child) {
                $item[$childKey][] = $this->resolveRow($child, $columns,$parentId);
            }
        }

        collect($columns)->each(function (Column $column) use ($row, &$item) {
            $columnNew = clone $column;
            $columnData = $columnNew->setRow($row);
            $item[$columnNew->name] = $columnData;

        });
        if($this->virtualize ){
            if(isset($item['id'])){
                $item['_id'] = $item['id'];
            }
            $item['id'] = Str::random(5);
        }


        return $item;
    }

}
