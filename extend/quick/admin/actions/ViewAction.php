<?php
declare (strict_types=1);

namespace quick\admin\actions;


use quick\admin\annotation\AdminAuth;
use quick\admin\http\model\Model;
use quick\admin\http\response\JsonResponse;
use think\facade\Db;

/**
 *
 * @AdminAuth(auth=true,menu=true,login=true,title="View",log=true)
 * @package quick\actions
 */
class ViewAction extends RowAction
{

    public $name = "查看";



    protected function handle($model, $request)
    {

        return  $response = $this->response()->success();
    }


    /**
     * @param $request
     * @param $model
     * @return \quick\admin\http\response\JsonResponse
     */
    protected function resolve($request, $model)
    {
        $form = $this->form();
        $form->fixedFooter();
        $form->hideReset();
        $form->hideSubmit();
        $form->url($this->storeUrl([
            self::$keyName => $request->param(self::$keyName)
        ]));
        $form->resolve($model);
        $form->style("background-color", '#FFFFFF');
        $form = $this->resolveComponent($form);
        return $this->response()->success('success', $form);
    }


}
