<?php
declare (strict_types=1);

namespace quick\admin\form\fields;


use app\common\model\SystemArea;
use Closure;
use quick\admin\Element;
use quick\admin\metable\Metable;
use think\Exception;

class SelectTag extends Field
{


    public $component = 'form-select-tag-field';

    /**
     * @var string
     */
    protected $valueType = 'array';

    /**
     * @var
     */
    protected $max;

    /**
     * @var
     */
    protected $min;

    /**
     * @var
     */
    public $default;


    /**
     * @var
     */
    protected $options;

    /**
     * @var string
     */
    protected $keyName;



    /**
     * @var array
     */
    protected $props = [];



//    public function transform($value)
//    {
//        if($this->cascaderType = 'region'){
//            $value = SystemArea::getValuesByIds($value);
//            return $value;
//        }
//    }



    /**
     * 模板弹出选择
     * @param string $load 加载地址
     * @param string $title 按钮名称
     * @param bool $isPush 追加模式
     * @return $this
     */
    public function load(string $load, string $title = 'select', bool $isPush = false): self
    {
        $this->props([
            'load' => $load,
            'loadTitle' => $title,
            'loadMode' => $isPush ? 'push' : 'submit',
        ]);
        return $this;
    }

    /**
     * 最小个数
     *
     * @param int $num
     * @return $this
     */
    public function min(int $num): self
    {
        $this->min = $num;
        $this->rules('min:' . $num);
        return $this;
    }


    /**
     * 最大个数
     *
     * @param int $num
     * @return $this
     */
    public function max(int $num): self
    {
        $this->max = $num;
        $this->rules('max:' . $num);
        return $this;
    }

    /**
     * 按钮名称
     * @param string $name
     * @return $this
     */
    public function btnName(string $name)
    {
        $this->props([
            'btnName' => $name,
        ]);
        return $this;
    }

    /**
     * 固定值配置
     * @param array $value 选中固定值
     * @param array $default 取消固定值时赋值默认值
     * @param string $text
     * @return $this
     */
    public function fixedValue(array $value = [],array $default = [],string $text)
    {
        $this->props([
            'width' => 'auto',
            'fixedValue' => $value,
            'fixedText' => $text,
            'fixedDefault' => $default,
        ]);
        return $this;
    }


    /**
     *
     * @return array
     */
    public function jsonSerialize(): array
    {
        $this->attribute('props', $this->props);
        $this->props([
            'min' => $this->min ?: 0,
            'max' => $this->max ?: 0,
        ]);
        return array_merge(parent::jsonSerialize(), []);
    }
}
