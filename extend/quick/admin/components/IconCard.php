<?php
declare (strict_types=1);

namespace quick\admin\components;


use quick\admin\Element;

/**
 *
 */
class IconCard extends Element
{


    public $component = 'icon-card';


    /**
     * @param string $title
     * @param string $number
     * @param string $icon
     */
    public function __construct(string $title, string $number, string $icon = '')
    {
        $this->title($title);
        $this->number($number);
        $icon && $this->icon($icon);
        $this->modeFull();
    }

    /**
     * 标题
     * @param string $title
     * @return $this
     */
    public function title(string $title): IconCard
    {
        $this->props("title", $title);
        return $this;
    }


    /**
     * 数量
     * @param string $number
     * @return $this
     */
    public function number(string $number): IconCard
    {
        $this->props("number", $number);
        return $this;
    }


    /**
     * @param string $icon 图标
     * @param string $iconInnerBackground 内圆背景色
     * @param string $iconOuterBackground 外圆背景色
     * @return $this
     */
    public function icon(string $icon,string $iconInnerBackground = '',string $iconOuterBackground = ''): IconCard
    {
        $this->props("icon", $icon);
        $iconInnerBackground && $this->iconInnerBackground($iconInnerBackground);
        $iconOuterBackground && $this->iconOuterBackground($iconOuterBackground);
        return $this;
    }


    /**
     * @param string $value
     * @param array $tagProps
     * @return $this
     */
    public function tagText(string $value,array $tagProps = []): IconCard
    {
        $this->props(__FUNCTION__, $value);
        !empty($tagProps) && $this->props('tagProps', $tagProps);
        return $this;
    }


    /**
     * compareTitle
     * @param string $value
     * @return $this
     */
    public function compareTitle(string $value): IconCard
    {
        $this->props(__FUNCTION__, $value);
        return $this;
    }


    /**
     * compareText
     * @param string $value
     * @return $this
     */
    public function compareText(string $value): IconCard
    {
        $this->props(__FUNCTION__, $value);
        return $this;
    }

    /**
     * compareColor
     * @param string $value
     * @return $this
     */
    public function compareColor(string $value): IconCard
    {
        $this->props(__FUNCTION__, $value);
        return $this;
    }

    /**
     * compareIcon
     * @param string $value
     * @return $this
     */
    public function compareIcon(string $value): IconCard
    {
        $this->props(__FUNCTION__, $value);
        return $this;
    }

    /**
     * iconOuterBackground
     * @param string $value
     * @return $this
     */
    public function iconOuterBackground(string $value): IconCard
    {
        $this->props(__FUNCTION__, $value);
        return $this;
    }

    /**
     * iconInnerBackground
     * @param string $value
     * @return $this
     */
    public function iconInnerBackground(string $value): IconCard
    {
        $this->props(__FUNCTION__, $value);
        return $this;
    }


    /**
     * @return $this
     */
    public function iconRight(): IconCard
    {
        $this->props('iconAlign', 'right');
        return $this;
    }

    /**
     * @return $this
     */
    public function iconLeft(): IconCard
    {
        $this->props('iconAlign', 'left');
        return $this;
    }


    /**
     * iconAlign
     * @param string $value right/left
     * @return $this
     */
    public function iconAlign(string $value): IconCard
    {
        $this->props(__FUNCTION__, $value);
        return $this;
    }



    /**
     * @return $this
     */
    public function modeMini(): IconCard
    {
        $this->props('mode', 'mini');
        return $this;
    }


    /**
     * @return $this
     */
    public function modeFull(): IconCard
    {
        $this->props('mode', 'full');
        return $this;
    }


    /**
     * 红色主题
     * @return $this
     */
    public function themeRed(): IconCard
    {
        $this->iconOuterBackground('#fff3e0');
        $this->iconInnerBackground('#ed4014');
        return $this;
    }

    /**
     * 蓝色主题
     * @return $this
     */
    public function themeBlue(): IconCard
    {
        $this->iconOuterBackground('#deeeff');
        $this->iconInnerBackground('#409EFF');
        return $this;
    }


    /**
     * @return $this
     */
    public function up(): IconCard
    {
//        $this->compareText($compareText);
//        $this->compareTitle($compareTitle);
        $this->compareColor("#ed4014");
        $this->compareIcon("el-icon-CaretTop");
        return $this;
    }

    /**
     * @return $this
     */
    public function down(): IconCard
    {
//        $this->compareText($compareText);
//        $this->compareTitle($compareTitle);
        $this->compareColor("#67C23A");
        $this->compareIcon("el-icon-CaretBottom");
        return $this;
    }


    /**
     * @param int $num
     * @return $this
     */
    public function percentageNum(int $num): IconCard
    {
        $this->props(__FUNCTION__, $num);
        return $this;
    }

    /**
     * @param int $num
     * @return $this
     */
    public function percentageText(string $value): IconCard
    {
        $this->props(__FUNCTION__, $value);
        return $this;
    }

    /**
     * @param array $value
     * @return $this
     */
    public function percentageProps(array $value): IconCard
    {
        $this->props(__FUNCTION__, $value);
        return $this;
    }


    /**
     * @param string $num
     * @return $this
     */
    public function percentageTitle(string $value): IconCard
    {
        $this->props(__FUNCTION__, $value);
        return $this;
    }
}
