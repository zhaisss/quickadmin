

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for qk_system_admin_info
-- ----------------------------
DROP TABLE IF EXISTS `qk_system_admin_info`;
CREATE TABLE `qk_system_admin_info`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '员工id',
  `user_id` int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '账号id',
  `plugin_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '模块标识',
  `auth_set` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  COMMENT '权限',
  `email` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '员工邮箱',
  `phone` varchar(15) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '员工手机号',
  `name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '员工姓名',
  `nickname` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '员工昵称',
  `avatar` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '员工头像',
  `gender` enum('male','female','unknow') CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT 'unknow' COMMENT '员工性别',
  `status` tinyint(1) NOT NULL DEFAULT 1 COMMENT '状态:1启用,0:禁用',
  `is_super_admin` tinyint(1) NOT NULL DEFAULT 0 COMMENT '超级管理员',
  `is_deleted` tinyint(1) NOT NULL DEFAULT 0 COMMENT '是否删除:0=未删除,1=删除',
  `created_at` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建日期',
  `updated_at` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新日期',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_id`(`user_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '系统管理员-员工' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of qk_system_admin_info
-- ----------------------------
-- INSERT INTO `qk_system_admin_info` VALUES (1, 45, 'admin', '', '', '', 'admin', 'admin', '', 'unknow', 1, 1, 0, '2022-07-20 14:59:47', '2022-12-12 16:31:01');

-- ----------------------------
-- Table structure for qk_system_area
-- ----------------------------
DROP TABLE IF EXISTS `qk_system_area`;
CREATE TABLE `qk_system_area`  (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `pid` int(10) NULL DEFAULT NULL COMMENT '父id',
  `shortname` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '简称',
  `name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '名称',
  `mergename` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '全称',
  `level` tinyint(4) NULL DEFAULT NULL COMMENT '层级:0=省,1=市,2=区县',
  `pinyin` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '拼音',
  `code` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '长途区号',
  `zip` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '邮编',
  `first` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '首字母',
  `lng` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '经度',
  `lat` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '纬度',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `pid`(`pid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '地区表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for qk_system_attachment
-- ----------------------------
DROP TABLE IF EXISTS `qk_system_attachment`;
CREATE TABLE `qk_system_attachment`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `storage_id` int(11) NOT NULL COMMENT '仓储ID',
  `attachment_cate_id` int(11) NOT NULL DEFAULT 0,
  `name` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '名称',
  `size` int(11) NOT NULL COMMENT '大小字节',
  `image` varchar(2080) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `thumb_image` varchar(2080) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '',
  `type` tinyint(2) NOT NULL COMMENT '类型:1=图片,2=视频',
  `is_recycle` tinyint(1) NOT NULL DEFAULT 0 COMMENT '加入回收站:0=否,1=是',
  `is_deleted` tinyint(1) NOT NULL DEFAULT 0 COMMENT '删除:0=未删除,1=已删除',
  `deleted_at` int(11) NOT NULL DEFAULT 0 COMMENT '删除日期',
  `created_at` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建日期',
  `updated_at` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新日期',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_cate`(`attachment_cate_id`) USING BTREE,
  INDEX `idx_type`(`type`) USING BTREE,
  INDEX `idx_deleted`(`is_deleted`) USING BTREE,
  INDEX `idx_recycle`(`is_recycle`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '附件表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for qk_system_attachment_cate
-- ----------------------------
DROP TABLE IF EXISTS `qk_system_attachment_cate`;
CREATE TABLE `qk_system_attachment_cate`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `storage_id` int(11) NOT NULL DEFAULT 0,
  `parent_id` int(11) NOT NULL DEFAULT 0 COMMENT '父级ID',
  `name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '名称',
  `is_recycle` tinyint(1) NOT NULL DEFAULT 0 COMMENT '加入回收站:0=否,1=是',
  `is_deleted` tinyint(1) NOT NULL DEFAULT 0 COMMENT '删除:0=未删除,1=已删除',
  `deleted_at` int(11) NOT NULL DEFAULT 0 COMMENT '删除日期',
  `created_at` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建日期',
  `updated_at` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新日期',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_parent_id`(`parent_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '附件分类表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for qk_system_attachment_storage
-- ----------------------------
DROP TABLE IF EXISTS `qk_system_attachment_storage`;
CREATE TABLE `qk_system_attachment_storage`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `plugin_name` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '插件标识',
  `user_id` int(11) NOT NULL DEFAULT 0,
  `is_deleted` tinyint(1) NOT NULL DEFAULT 0 COMMENT '删除:0=未删除,1=已删除',
  `deleted_at` int(11) NOT NULL DEFAULT 0 COMMENT '删除日期',
  `created_at` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建日期',
  `updated_at` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新日期',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_id`(`user_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '附件仓库表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for qk_system_auth
-- ----------------------------
DROP TABLE IF EXISTS `qk_system_auth`;
CREATE TABLE `qk_system_auth`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `pid` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '父id',
  `plugin_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '系统插件plugin_name',
  `name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '角色名称',
  `desc` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '说明',
  `level` smallint(5) NOT NULL DEFAULT 1 COMMENT '管理级别',
  `status` tinyint(1) UNSIGNED NOT NULL DEFAULT 1 COMMENT '状态(0:禁用,1:启用)',
  `deleted_at` int(11) NOT NULL DEFAULT 0 COMMENT '删除日期',
  `created_at` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建日期',
  `updated_at` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新日期',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_plugin_id`(`plugin_name`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '角色表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for qk_system_auth_node
-- ----------------------------
DROP TABLE IF EXISTS `qk_system_auth_node`;
CREATE TABLE `qk_system_auth_node`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `auth` bigint(20) UNSIGNED NULL DEFAULT 0 COMMENT '角色',
  `node_id` int(10) NULL DEFAULT 0,
  `node` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '节点',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_system_auth_auth`(`auth`) USING BTREE,
  INDEX `idx_system_auth_node`(`node`(191)) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '系统-授权' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for qk_system_config
-- ----------------------------
DROP TABLE IF EXISTS `qk_system_config`;
CREATE TABLE `qk_system_config`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `group` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '分组',
  `plugin` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT 'admin' COMMENT '插件标识',
  `config_type` enum('code','admin') CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT 'admin' COMMENT '配置类型:code=代码配置,admin=后台配置',
  `type` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '类型:string,text,int,bool,json,datetime,date,file',
  `title` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '配置标题',
  `desc` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '配置简介',
  `name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '配置名',
  `value` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '配置值',
  `content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '变量字典数据',
  `rule` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '验证规则',
  `status` tinyint(1) NOT NULL DEFAULT 1 COMMENT '状态:1=显示,0=隐藏',
  `sort` int(10) NOT NULL DEFAULT 1 COMMENT '排序',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `name`(`name`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 43 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '系统-配置' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of qk_system_config
-- ----------------------------
INSERT INTO `qk_system_config` VALUES (4, 'base', 'admin', 'admin', 'upload_image', '系统logo', '', 'app_logo', 'http://demo.quickadmin.cn/upload/e9/d8b00136b434694ae5c7d9070dcab5.png', '{\"value1\":\"title1\",\"value2\":\"title2\"}', '', 1, 0);
INSERT INTO `qk_system_config` VALUES (5, 'alioss', 'admin', 'admin', 'text', 'alioss_point', NULL, 'alioss_point', '', '{\"value1\":\"title1\",\"value2\":\"title2\"}', '', 1, 0);
INSERT INTO `qk_system_config` VALUES (6, 'alioss', 'admin', 'admin', 'text', 'alioss_bucket', NULL, 'alioss_bucket', '', '{\"value1\":\"title1\",\"value2\":\"title2\"}', '', 1, 0);
INSERT INTO `qk_system_config` VALUES (7, 'alioss', 'admin', 'admin', 'text', 'alioss_access_key', NULL, 'alioss_access_key', '', '{\"value1\":\"title1\",\"value2\":\"title2\"}', '', 1, 0);
INSERT INTO `qk_system_config` VALUES (8, 'alioss', 'admin', 'admin', 'text', 'alioss_secret_key', NULL, 'alioss_secret_key', '', '{\"value1\":\"title1\",\"value2\":\"title2\"}', '', 1, 0);
INSERT INTO `qk_system_config` VALUES (9, 'alioss', 'admin', 'admin', 'radio', 'alioss_http_protocol', '', 'alioss_http_protocol', 'https', 'https=https\nhttp=http', '', 1, 99);
INSERT INTO `qk_system_config` VALUES (10, 'alioss', 'admin', 'admin', 'text', 'alioss_http_domain', NULL, 'alioss_http_domain', '', '{\"value1\":\"title1\",\"value2\":\"title2\"}', '', 1, 3);
INSERT INTO `qk_system_config` VALUES (11, 'storage', 'admin', 'admin', 'radio', '储存类型', '', 'storage_type', 'local', 'local=本地\nalioss=阿里云\r\ntxcos=腾讯云', '', 1, 100);
INSERT INTO `qk_system_config` VALUES (12, 'base', 'admin', 'admin', 'text', '系统名称', NULL, 'app_name', 'QuickAdmin', '{\"value1\":\"title1\",\"value2\":\"title2\"}', '', 1, 1);
INSERT INTO `qk_system_config` VALUES (19, 'wxapp', 'admin', 'admin', 'text', '小程序AppId', '', 'appid', 'wxa', '', '', 1, 1);
INSERT INTO `qk_system_config` VALUES (20, 'wxapp', 'admin', 'admin', 'text', '小程序AppSecret', '', 'app_secret', '8287f79885', '', '', 1, 1);
INSERT INTO `qk_system_config` VALUES (21, 'base', 'mall', 'code', 'string', '基础设置', '配置', 'sted', '2', NULL, '', 1, 1);
INSERT INTO `qk_system_config` VALUES (22, 'base', 'mall', 'code', 'string', '基础设置', '配置', 'tesst', '3', NULL, '', 1, 1);
INSERT INTO `qk_system_config` VALUES (24, 'show', 'mall', 'code', 'string', '显示配置', '显示配置', 'sted', '56', NULL, '', 1, 1);
INSERT INTO `qk_system_config` VALUES (25, 'show', 'mall', 'code', 'string', '显示配置', '显示配置', 'tesst', '77', NULL, '', 1, 1);
INSERT INTO `qk_system_config` VALUES (26, 'base3', 'mall', 'code', 'string', '支付方式', '支付方式', 'test', '343434', NULL, '', 1, 1);
INSERT INTO `qk_system_config` VALUES (27, 'order', 'mall', 'code', 'string', '订单配置', '订单配置', 'sted', '4', NULL, '', 1, 1);
INSERT INTO `qk_system_config` VALUES (28, 'order', 'mall', 'code', 'string', '订单配置', '订单配置', 'tesst', '6', NULL, '', 1, 1);
INSERT INTO `qk_system_config` VALUES (29, 'system_mail', 'admin', 'admin', 'text', 'SMTP 服务器', '', 'mail_server', '', NULL, '', 1, 1);
INSERT INTO `qk_system_config` VALUES (30, 'system_mail', 'admin', 'admin', 'text', 'SMTP端口', '', 'mail_port', '465', NULL, '', 1, 1);
INSERT INTO `qk_system_config` VALUES (31, 'system_mail', 'admin', 'admin', 'text', 'SMTP 用户名', '', 'mail_user', '', NULL, '', 1, 1);
INSERT INTO `qk_system_config` VALUES (32, 'system_mail', 'admin', 'admin', 'radio', 'SMTP 验证方式', '', 'mail_verification', 'ssl', 'ssl=SSL\ntls=TLS\n', '', 1, 1);
INSERT INTO `qk_system_config` VALUES (33, 'system_mail', 'admin', 'admin', 'text', 'SMTP 发件人邮箱', '', 'mail_sender_mail', '', NULL, '', 1, 1);
INSERT INTO `qk_system_config` VALUES (34, 'system_mail', 'admin', 'admin', 'text', 'SMTP 密码', '', 'mail_password', '', NULL, '', 1, 1);
INSERT INTO `qk_system_config` VALUES (36, 'txcos', 'admin', 'admin', 'text', '存储区域', '腾讯云COS存储空间所在区域，需要严格对应储存所在区域才能上传文件', 'txcos_point', '', '', '', 1, 0);
INSERT INTO `qk_system_config` VALUES (37, 'txcos', 'admin', 'admin', 'text', '空间名称', '填写腾讯云COS存储空间名称，如：field-test', 'txcos_bucket', '', '', '', 1, 0);
INSERT INTO `qk_system_config` VALUES (38, 'txcos', 'admin', 'admin', 'text', '访问密钥', '可以在 [ 腾讯云 > 个人中心 ] 设置并获取到访问密钥', 'txcos_access_key', '', '', '', 1, 0);
INSERT INTO `qk_system_config` VALUES (39, 'txcos', 'admin', 'admin', 'text', '安全密钥', '可以在 [ 腾讯云 > 个人中心 ] 设置并获取到安全密钥', 'txcos_secret_key', '', '', '', 1, 0);
INSERT INTO `qk_system_config` VALUES (40, 'txcos', 'admin', 'admin', 'radio', '访问协议', ' 腾讯云COS存储访问协议，其中 HTTPS 需要配置证书才能使用（ AUTO 为相对协议 ）', 'txcos_http_protocol', '', 'https=https\r\nhttp=http', '', 1, 80);
INSERT INTO `qk_system_config` VALUES (41, 'txcos', 'admin', 'admin', 'text', '访问域名', '填写腾讯云COS存储外部访问域名，如：static.txcos.quickadmin.cn', 'txcos_http_domain', '', '', '', 1, 99);
INSERT INTO `qk_system_config` VALUES (42, 'admin', 'admin', 'code', 'string', '数据库版本', '数据库版本,用于标记系统升级版本号', 'quick_sql_version', '1.0.1', NULL, '', 1, 1);
INSERT INTO `qk_system_config` VALUES (43, 'storage', 'admin', 'admin', 'text', '文件前缀', '', 'storage_dir_prefix', '', NULL, '', 1, 1);
-- ----------------------------
-- Table structure for qk_system_config_group
-- ----------------------------
DROP TABLE IF EXISTS `qk_system_config_group`;
CREATE TABLE `qk_system_config_group`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `parent_id` int(11) NOT NULL DEFAULT 0 COMMENT '父级ID',
  `group` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '分组变量名称',
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '分组别名',
  `status` tinyint(1) UNSIGNED NOT NULL DEFAULT 1 COMMENT '状态:1=启用,0=禁用',
  `show` tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '显示:1=显示,0=隐藏',
  `type` int(2) NULL DEFAULT 0 COMMENT '分组类型',
  `sort` int(11) NOT NULL DEFAULT 100 COMMENT '排序',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 35 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '系统-配置-分组' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of qk_system_config_group
-- ----------------------------
INSERT INTO `qk_system_config_group` VALUES (3, 0, 'wxapp', '小程序配置', 1, 0, 0, 100);
INSERT INTO `qk_system_config_group` VALUES (28, 0, 'quick', '系统配置', 1, 0, 0, 200);
INSERT INTO `qk_system_config_group` VALUES (29, 0, 'upload', '上传配置', 1, 0, 0, 99);
INSERT INTO `qk_system_config_group` VALUES (30, 28, 'base', '基本配置', 1, 0, 0, 100);
INSERT INTO `qk_system_config_group` VALUES (31, 28, 'system_mail', '邮箱配置', 1, 0, 0, 100);
INSERT INTO `qk_system_config_group` VALUES (32, 29, 'storage', '默认配置', 1, 0, 0, 100);
INSERT INTO `qk_system_config_group` VALUES (33, 29, 'txcos', '腾讯云', 1, 0, 0, 100);
INSERT INTO `qk_system_config_group` VALUES (34, 29, 'alioss', '阿里云', 1, 0, 0, 100);

-- ----------------------------
-- Table structure for qk_system_group
-- ----------------------------
DROP TABLE IF EXISTS `qk_system_group`;
CREATE TABLE `qk_system_group`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '组合数据ID',
  `plugin` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '模块插件',
  `title` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '数据组名称',
  `info` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '数据提示',
  `name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '数据字段名称',
  `fields` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '数据组字段（json数据）',
  `created_at` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建日期',
  `updated_at` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新日期',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `idx_name`(`name`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '组合数据表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for qk_system_group_data
-- ----------------------------
DROP TABLE IF EXISTS `qk_system_group_data`;
CREATE TABLE `qk_system_group_data`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `plugin` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '模块插件',
  `group` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '0' COMMENT '数据组',
  `value` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '数据组数据（json数据）',
  `sort` int(11) NOT NULL DEFAULT 0 COMMENT '数据排序',
  `status` tinyint(1) NOT NULL DEFAULT 1 COMMENT '状态1：开启；0：关闭',
  `created_at` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建日期',
  `updated_at` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新日期',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_plugin_name`(`plugin`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '组合数据详情' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for qk_system_jobs
-- ----------------------------
DROP TABLE IF EXISTS `qk_system_jobs`;
CREATE TABLE `qk_system_jobs`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '自增主键',
  `queue` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '队列名称',
  `payload` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '有效负载',
  `attempts` tinyint(3) UNSIGNED NOT NULL DEFAULT 0 COMMENT '重试次数',
  `reserved` tinyint(3) UNSIGNED NOT NULL DEFAULT 0 COMMENT '订阅次数',
  `reserve_time` int(10) UNSIGNED NULL DEFAULT 0 COMMENT '订阅时间',
  `available_time` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '有效时间',
  `create_time` int(10) NOT NULL DEFAULT 0 COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '系统-消息列的' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for qk_system_menu
-- ----------------------------
DROP TABLE IF EXISTS `qk_system_menu`;
CREATE TABLE `qk_system_menu`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `plugin_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '系统插件plugin_name ',
  `pid` bigint(20) UNSIGNED NULL DEFAULT 0 COMMENT '上级ID',
  `title` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '菜单名称',
  `icon` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '菜单图标',
  `badge` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT 'badge',
  `node` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '节点代码',
  `path` varchar(400) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '链接节点',
  `params` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '链接参数',
  `target` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '_self' COMMENT '打开方式 _blank _self',
  `sort` int(11) UNSIGNED NULL DEFAULT 0 COMMENT '排序权重',
  `is_admin` tinyint(1) UNSIGNED NULL DEFAULT 0 COMMENT '平台菜单 1',
  `level` smallint(5) UNSIGNED NULL DEFAULT 1 COMMENT '管理级别',
  `status` tinyint(1) UNSIGNED NULL DEFAULT 1 COMMENT '状态(0:禁用,1:启用)',
  `created_at` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建日期',
  `updated_at` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新日期',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_node`(`node`) USING BTREE,
  INDEX `idx_status`(`status`) USING BTREE,
  INDEX `idx_plugin_name`(`plugin_name`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 168 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '系统菜单' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of qk_system_menu
-- ----------------------------
INSERT INTO `qk_system_menu` VALUES (3, 'admin', 64, '菜单管理', 'redenvelope-fill', '', '', 'admin/resource/menus/index', '', '_self', 0, 0, 1, 1, '2021-06-22 22:52:17', '2021-06-22 22:52:17');
INSERT INTO `qk_system_menu` VALUES (10, 'admin', 75, '权限管理', 'el-icon-Coin', '', '', '#', '', '_self', 20, 0, 1, 1, '2022-01-13 11:42:34', '2022-01-13 11:42:34');
INSERT INTO `qk_system_menu` VALUES (14, 'admin', 10, '权限管理', 'radius-bottomright', '', '', 'admin/resource/auth/index', '', '_self', 0, 0, 1, 1, '2021-10-02 10:45:14', '2021-10-02 10:45:14');
INSERT INTO `qk_system_menu` VALUES (16, 'admin', 10, '节点管理', 'interation-fill', '', '', 'admin/resource/node/index', '', '_self', 0, 0, 1, 0, '2022-01-21 14:57:35', '2022-03-15 16:51:22');
INSERT INTO `qk_system_menu` VALUES (23, 'admin', 75, '首页', 'el-icon-house', '', '', 'dashboard', '', '_self', 25, 0, 1, 1, '2022-01-21 14:59:52', '2022-01-21 14:59:52');
INSERT INTO `qk_system_menu` VALUES (27, 'admin', 64, '系统日志', 'deleteteam', '', '', 'admin/resource/oplog/index', '', '_self', 7, 0, 1, 1, '2021-06-22 22:51:58', '2021-06-22 22:51:58');
INSERT INTO `qk_system_menu` VALUES (28, 'admin', 10, '管理员工', 'project-fill', '', '', 'admin/resource/admin/index', '', '_self', 1, 0, 1, 1, '2021-10-01 12:20:30', '2021-10-01 12:20:30');
INSERT INTO `qk_system_menu` VALUES (64, 'admin', 75, '系统配置', 'el-icon-Setting', '', '', '#', '', '_self', 22, 0, 1, 1, '2022-01-13 11:42:04', '2022-01-13 11:42:04');
INSERT INTO `qk_system_menu` VALUES (65, 'admin', 64, '系统配置', 'border-inner', '', '', 'admin/resource/system_config/index', '', '_self', 1, 0, 1, 1, '2021-10-07 20:55:34', '2021-10-07 20:55:34');
INSERT INTO `qk_system_menu` VALUES (67, 'admin', 84, '系统任务', '', '', '', 'admin/resource/system_queue/index', '', '_self', 1, 0, 1, 1, '2021-10-03 16:06:37', '2022-04-17 21:58:49');
INSERT INTO `qk_system_menu` VALUES (69, 'admin', 84, '组合配置', '', '', '', 'admin/resource/group_new/index', '', '_self', 1, 0, 1, 1, '2022-01-20 14:31:58', '2022-01-20 14:31:58');
INSERT INTO `qk_system_menu` VALUES (70, 'admin', 64, '组合数据', 'border-verticle', '', '', 'admin/resource/group/index', '', '_self', 1, 0, 1, 0, '2021-12-27 16:29:20', '2021-12-27 16:29:20');
INSERT INTO `qk_system_menu` VALUES (75, 'admin', 0, '系统管理', 'el-icon-Setting', '', '', 'admin', '', '_self', 400, 0, 1, 1, '2022-01-23 21:59:51', '2022-01-23 21:59:51');
INSERT INTO `qk_system_menu` VALUES (84, 'admin', 75, '系统维护', 'el-icon-Setting', '', '', '#', '', '_self', 1, 0, 1, 1, '2022-01-04 20:28:26', '2022-01-04 20:28:26');
INSERT INTO `qk_system_menu` VALUES (85, 'admin', 84, '代码crud', '', '', '', 'crud/resource/index/index', '', '_self', 4, 0, 1, 1, '2022-01-17 16:58:27', '2022-01-17 16:58:27');
INSERT INTO `qk_system_menu` VALUES (87, 'admin', 84, '系统配置', '', '', '', 'admin/resource/config/index', '', '_self', 1, 0, 1, 1, '2022-01-19 09:12:28', '2022-12-06 16:35:00');
INSERT INTO `qk_system_menu` VALUES (88, 'admin', 75, '用户管理', 'el-icon-User', '', '', 'admin/resource/system_user/index', '', '_self', 1, 0, 1, 0, '2022-01-28 17:29:00', '2022-04-12 16:06:44');
INSERT INTO `qk_system_menu` VALUES (167, 'admin', 84, '系统更新', '', '', '', 'admin/resource/upgrade/index', '', '_self', 1, 0, 1, 1, '2022-04-17 21:58:26', '2022-04-17 21:58:26');

-- ----------------------------
-- Table structure for qk_system_node
-- ----------------------------
DROP TABLE IF EXISTS `qk_system_node`;
CREATE TABLE `qk_system_node`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `plugin_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '系统插件plugin',
  `node` varchar(150) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '节点规则',
  `pnode` varchar(150) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '父节点',
  `mode` varchar(150) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '节点类型:controller、resource',
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '规则名称',
  `method` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '请求类型json',
  `condition` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '条件',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '备注',
  `level` tinyint(1) NOT NULL DEFAULT 1 COMMENT '节点层级',
  `is_menu` tinyint(1) UNSIGNED NULL DEFAULT 0 COMMENT '是否可设置为菜单',
  `is_auth` tinyint(1) UNSIGNED NULL DEFAULT 1 COMMENT '是否启动RBAC权限控制',
  `is_login` tinyint(1) UNSIGNED NULL DEFAULT 1 COMMENT '是否启动登录控制',
  `is_log` tinyint(1) NULL DEFAULT 0 COMMENT '记录日志',
  `status` tinyint(1) UNSIGNED NULL DEFAULT 1 COMMENT '状态(0:禁用,1:启用)',
  `sort` int(11) UNSIGNED NULL DEFAULT 0 COMMENT '排序',
  `created_at` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建日期',
  `updated_at` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新日期',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `node`(`node`) USING BTREE,
  INDEX `idx_node`(`node`) USING BTREE,
  INDEX `idx_plugin_id`(`plugin_name`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 145 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '节点表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of qk_system_node
-- ----------------------------
INSERT INTO `qk_system_node` VALUES (1, 'admin', 'admin', '', 'resource', '系统管理', '[]', '', '', 1, 1, 1, 1, 0, 1, 0, '2022-12-12 16:29:56', '2022-12-12 16:29:56');
INSERT INTO `qk_system_node` VALUES (2, 'admin', 'admin/index', 'admin', 'controller', '系统管理', '[]', '', '', 2, 1, 1, 0, 0, 1, 0, '2022-12-12 16:29:56', '2022-12-12 16:29:56');
INSERT INTO `qk_system_node` VALUES (3, 'admin', 'admin/index/index', 'admin/index', 'controller', 'Index', '[]', '', '', 3, 0, 0, 0, 0, 1, 0, '2022-12-12 16:29:56', '2022-12-12 16:29:56');
INSERT INTO `qk_system_node` VALUES (4, 'admin', 'admin/index/menu', 'admin/index', 'controller', '菜单', '[]', '', '', 3, 1, 1, 1, 0, 1, 0, '2022-12-12 16:29:56', '2022-12-12 16:29:56');
INSERT INTO `qk_system_node` VALUES (5, 'admin', 'admin/index/clearcache', 'admin/index', 'controller', '清除缓存', '[]', '', '', 3, 1, 1, 1, 0, 1, 0, '2022-12-12 16:29:56', '2022-12-12 16:32:30');
INSERT INTO `qk_system_node` VALUES (6, 'admin', 'admin/index/logout', 'admin/index', 'controller', '退出登录', '[]', '', '', 3, 1, 1, 1, 0, 1, 0, '2022-12-12 16:29:56', '2022-12-12 16:29:56');
INSERT INTO `qk_system_node` VALUES (7, 'admin', 'admin/passport', 'admin', 'controller', '登录系统', '[]', '', '', 2, 1, 0, 1, 0, 1, 0, '2022-12-12 16:29:56', '2022-12-12 16:29:56');
INSERT INTO `qk_system_node` VALUES (8, 'admin', 'admin/passport/login', 'admin/passport', 'controller', '登录', '[]', '', '', 3, 0, 0, 0, 1, 1, 0, '2022-12-12 16:29:56', '2022-12-12 16:29:56');
INSERT INTO `qk_system_node` VALUES (9, 'admin', 'admin/passport/userinfo', 'admin/passport', 'controller', '员工数据', '[]', '', '', 3, 0, 0, 1, 0, 1, 0, '2022-12-12 16:29:56', '2022-12-12 16:29:56');
INSERT INTO `qk_system_node` VALUES (10, 'admin', 'admin/update', 'admin', 'controller', '升级管理', '[]', '', '', 2, 0, 0, 0, 0, 1, 0, '2022-12-12 16:29:56', '2022-12-12 16:29:56');
INSERT INTO `qk_system_node` VALUES (11, 'admin', 'admin/update/get', 'admin/update', 'controller', '升级管理', '[]', '', '', 3, 0, 0, 0, 0, 1, 0, '2022-12-12 16:29:56', '2022-12-12 16:29:56');
INSERT INTO `qk_system_node` VALUES (12, 'admin', 'admin/update/node', 'admin/update', 'controller', '升级管理', '[]', '', '', 3, 0, 0, 0, 0, 1, 0, '2022-12-12 16:29:56', '2022-12-12 16:29:56');
INSERT INTO `qk_system_node` VALUES (13, 'admin', 'admin/update/version', 'admin/update', 'controller', '升级管理1', '[]', '', '', 3, 0, 0, 0, 0, 1, 0, '2022-12-12 16:29:56', '2022-12-12 16:29:56');
INSERT INTO `qk_system_node` VALUES (14, 'crud', 'crud', '', 'resource', 'CRUD工具', '[]', '', '', 1, 1, 1, 1, 0, 1, 0, '2022-12-12 16:29:56', '2022-12-12 16:32:30');
INSERT INTO `qk_system_node` VALUES (15, 'crud', 'crud/api', 'crud', 'controller', 'CRUD工具', '[]', '', '', 2, 1, 1, 0, 0, 1, 0, '2022-12-12 16:29:56', '2022-12-12 16:29:56');
INSERT INTO `qk_system_node` VALUES (16, 'crud', 'crud/api/classlist', 'crud/api', 'controller', 'classList', '[]', '', '', 3, 1, 1, 1, 0, 1, 0, '2022-12-12 16:29:56', '2022-12-12 16:29:56');
INSERT INTO `qk_system_node` VALUES (17, 'crud', 'crud/api/update', 'crud/api', 'controller', 'update', '[]', '', '', 3, 1, 1, 1, 0, 1, 0, '2022-12-12 16:29:56', '2022-12-12 16:29:56');
INSERT INTO `qk_system_node` VALUES (18, 'crud', 'crud/index', 'crud', 'controller', 'CRUD工具', '[]', '', '', 2, 1, 1, 0, 0, 1, 0, '2022-12-12 16:29:56', '2022-12-12 16:29:56');
INSERT INTO `qk_system_node` VALUES (19, 'crud', 'crud/index/tablefields', 'crud/index', 'controller', '编辑分类', '[]', '', '', 3, 1, 1, 1, 0, 1, 0, '2022-12-12 16:29:56', '2022-12-12 16:29:56');
INSERT INTO `qk_system_node` VALUES (20, 'crud', 'crud/index/tablelist', 'crud/index', 'controller', 'tableList', '[]', '', '', 3, 1, 1, 1, 0, 1, 0, '2022-12-12 16:29:56', '2022-12-12 16:29:56');
INSERT INTO `qk_system_node` VALUES (21, 'crud', 'crud/index/previewform', 'crud/index', 'controller', 'previewForm', '[]', '', '', 3, 1, 1, 1, 0, 1, 0, '2022-12-12 16:29:56', '2022-12-12 16:29:56');
INSERT INTO `qk_system_node` VALUES (22, 'crud', 'crud/index/previewmodel', 'crud/index', 'controller', 'previewModel', '[]', '', '', 3, 1, 1, 1, 0, 1, 0, '2022-12-12 16:29:56', '2022-12-12 16:29:56');
INSERT INTO `qk_system_node` VALUES (23, 'crud', 'crud/index/previewtable', 'crud/index', 'controller', 'previewTable', '[]', '', '', 3, 1, 1, 1, 0, 1, 0, '2022-12-12 16:29:56', '2022-12-12 16:29:56');
INSERT INTO `qk_system_node` VALUES (24, 'crud', 'crud/index/previewresource', 'crud/index', 'controller', 'previewResource', '[]', '', '', 3, 1, 1, 1, 0, 1, 0, '2022-12-12 16:29:56', '2022-12-12 16:29:56');
INSERT INTO `qk_system_node` VALUES (25, 'crud', 'crud/index/createcrud', 'crud/index', 'controller', 'createCrud', '[]', '', '', 3, 1, 1, 1, 0, 1, 0, '2022-12-12 16:29:56', '2022-12-12 16:29:56');
INSERT INTO `qk_system_node` VALUES (26, 'crud', 'crud/index/previewaction', 'crud/index', 'controller', 'previewAction', '[]', '', '', 3, 1, 1, 1, 0, 1, 0, '2022-12-12 16:29:56', '2022-12-12 16:29:56');
INSERT INTO `qk_system_node` VALUES (27, 'crud', 'crud/index/createaction', 'crud/index', 'controller', 'createAction', '[]', '', '', 3, 1, 1, 1, 0, 1, 0, '2022-12-12 16:29:56', '2022-12-12 16:29:56');
INSERT INTO `qk_system_node` VALUES (28, 'crud', 'crud/index/createapi', 'crud/index', 'controller', 'createApi', '[]', '', '', 3, 1, 1, 1, 0, 1, 0, '2022-12-12 16:29:56', '2022-12-12 16:29:56');
INSERT INTO `qk_system_node` VALUES (29, 'crud', 'crud/index/menulist', 'crud/index', 'controller', 'menuList', '[]', '', '', 3, 1, 1, 1, 0, 1, 0, '2022-12-12 16:29:56', '2022-12-12 16:29:56');
INSERT INTO `qk_system_node` VALUES (30, 'admin', 'admin/resource/attachment', 'admin', 'resource', '素材管理', '[]', '', '', 2, 1, 1, 1, 0, 1, 0, '2022-12-12 16:29:56', '2022-12-12 16:29:56');
INSERT INTO `qk_system_node` VALUES (31, 'admin', 'admin/resource/attachment/editcate', 'admin/resource/attachment', 'resource', '编辑分类', '[]', '', '', 3, 1, 1, 1, 0, 1, 0, '2022-12-12 16:29:56', '2022-12-12 16:29:56');
INSERT INTO `qk_system_node` VALUES (32, 'admin', 'admin/resource/attachment/catelist', 'admin/resource/attachment', 'resource', '分类列表', '[]', '', '', 3, 1, 1, 1, 0, 1, 0, '2022-12-12 16:29:56', '2022-12-12 16:29:56');
INSERT INTO `qk_system_node` VALUES (33, 'admin', 'admin/resource/attachment/delcate', 'admin/resource/attachment', 'resource', '删除分类', '[]', '', '', 3, 1, 1, 1, 0, 1, 0, '2022-12-12 16:29:56', '2022-12-12 16:29:56');
INSERT INTO `qk_system_node` VALUES (34, 'admin', 'admin/resource/attachment/list', 'admin/resource/attachment', 'resource', '素材列表', '[]', '', '', 3, 1, 1, 1, 0, 1, 0, '2022-12-12 16:29:56', '2022-12-12 16:29:56');
INSERT INTO `qk_system_node` VALUES (35, 'admin', 'admin/resource/attachment/delete', 'admin/resource/attachment', 'resource', '删除图片', '[]', '', '', 3, 1, 1, 1, 0, 1, 0, '2022-12-12 16:29:56', '2022-12-12 16:29:56');
INSERT INTO `qk_system_node` VALUES (36, 'admin', 'admin/resource/attachment/update', 'admin/resource/attachment', 'resource', '修改图片名称', '[]', '', '', 3, 1, 1, 1, 0, 1, 0, '2022-12-12 16:29:56', '2022-12-12 16:29:56');
INSERT INTO `qk_system_node` VALUES (37, 'admin', 'admin/resource/attachment/recycle', 'admin/resource/attachment', 'resource', '放入回收站', '[]', '', '', 3, 1, 1, 1, 0, 1, 0, '2022-12-12 16:29:56', '2022-12-12 16:29:56');
INSERT INTO `qk_system_node` VALUES (38, 'admin', 'admin/resource/attachment/movecate', 'admin/resource/attachment', 'resource', '迁移', '[]', '', '', 3, 1, 1, 1, 0, 1, 0, '2022-12-12 16:29:56', '2022-12-12 16:29:56');
INSERT INTO `qk_system_node` VALUES (39, 'admin', 'admin/resource/attachment/upload', 'admin/resource/attachment', 'resource', '上传', '[]', '', '', 3, 1, 1, 1, 0, 1, 0, '2022-12-12 16:29:56', '2022-12-12 16:29:56');
INSERT INTO `qk_system_node` VALUES (40, 'admin', 'admin/resource/attachment/index', 'admin/resource/attachment', 'resource', '列表', '[]', '', '', 3, 1, 1, 1, 0, 1, 0, '2022-12-12 16:29:56', '2022-12-12 16:29:56');
INSERT INTO `qk_system_node` VALUES (41, 'admin', 'admin/resource/attachment/attachment_cate_list_action', 'admin/resource/attachment', 'resource', '设置密码', '[]', '', '', 3, 0, 1, 1, 0, 1, 0, '2022-12-12 16:29:56', '2022-12-12 16:29:56');
INSERT INTO `qk_system_node` VALUES (42, 'admin', 'admin/resource/admin', 'admin', 'resource', '系统管理员', '[]', '', '', 2, 1, 1, 1, 0, 1, 0, '2022-12-12 16:29:56', '2022-12-12 16:29:56');
INSERT INTO `qk_system_node` VALUES (43, 'admin', 'admin/resource/admin/searchuser', 'admin/resource/admin', 'resource', '搜索会员', '[]', '', '', 3, 1, 1, 1, 0, 1, 0, '2022-12-12 16:29:56', '2022-12-12 16:29:56');
INSERT INTO `qk_system_node` VALUES (44, 'admin', 'admin/resource/admin/index', 'admin/resource/admin', 'resource', '列表', '[]', '', '', 3, 1, 1, 1, 0, 1, 0, '2022-12-12 16:29:56', '2022-12-12 16:29:56');
INSERT INTO `qk_system_node` VALUES (45, 'admin', 'admin/resource/admin/switch_column_status', 'admin/resource/admin', 'resource', '启用状态', '[]', '', '', 3, 1, 1, 1, 1, 1, 0, '2022-12-12 16:29:56', '2022-12-12 16:29:56');
INSERT INTO `qk_system_node` VALUES (46, 'admin', 'admin/resource/admin/edit_action', 'admin/resource/admin', 'resource', '编辑', '[]', '', '', 3, 1, 1, 1, 1, 1, 0, '2022-12-12 16:29:56', '2022-12-12 16:29:56');
INSERT INTO `qk_system_node` VALUES (47, 'admin', 'admin/resource/admin/delete_action', 'admin/resource/admin', 'resource', '删除', '[]', '', '', 3, 1, 1, 1, 1, 1, 0, '2022-12-12 16:29:56', '2022-12-12 16:29:56');
INSERT INTO `qk_system_node` VALUES (48, 'admin', 'admin/resource/admin/edit_user_password_action', 'admin/resource/admin', 'resource', '设置密码', '[]', '', '', 3, 0, 1, 1, 0, 1, 0, '2022-12-12 16:29:56', '2022-12-12 16:29:56');
INSERT INTO `qk_system_node` VALUES (49, 'admin', 'admin/resource/admin/add_action', 'admin/resource/admin', 'resource', '添加', '[]', '', '', 3, 1, 1, 1, 1, 1, 0, '2022-12-12 16:29:56', '2022-12-12 16:29:56');
INSERT INTO `qk_system_node` VALUES (50, 'admin', 'admin/resource/auth', 'admin', 'resource', '权限管理', '[]', '', '', 2, 1, 1, 1, 0, 1, 0, '2022-12-12 16:29:56', '2022-12-12 16:29:56');
INSERT INTO `qk_system_node` VALUES (51, 'admin', 'admin/resource/auth/index', 'admin/resource/auth', 'resource', '列表', '[]', '', '', 3, 1, 1, 1, 0, 1, 0, '2022-12-12 16:29:56', '2022-12-12 16:29:56');
INSERT INTO `qk_system_node` VALUES (52, 'admin', 'admin/resource/auth/batch_delete_action', 'admin/resource/auth', 'resource', '批量删除', '[]', '', '', 3, 1, 1, 1, 1, 1, 0, '2022-12-12 16:29:56', '2022-12-12 16:29:56');
INSERT INTO `qk_system_node` VALUES (53, 'admin', 'admin/resource/auth/switch_column_status', 'admin/resource/auth', 'resource', '状态', '[]', '', '', 3, 1, 1, 1, 1, 1, 0, '2022-12-12 16:29:56', '2022-12-12 16:29:56');
INSERT INTO `qk_system_node` VALUES (54, 'admin', 'admin/resource/auth/edit_action', 'admin/resource/auth', 'resource', '编辑', '[]', '', '', 3, 1, 1, 1, 1, 1, 0, '2022-12-12 16:29:56', '2022-12-12 16:29:56');
INSERT INTO `qk_system_node` VALUES (55, 'admin', 'admin/resource/auth/delete_action', 'admin/resource/auth', 'resource', '删除', '[]', '', '', 3, 1, 1, 1, 1, 1, 0, '2022-12-12 16:29:56', '2022-12-12 16:29:56');
INSERT INTO `qk_system_node` VALUES (56, 'admin', 'admin/resource/auth/add_action', 'admin/resource/auth', 'resource', '添加', '[]', '', '', 3, 1, 1, 1, 1, 1, 0, '2022-12-12 16:29:56', '2022-12-12 16:29:56');
INSERT INTO `qk_system_node` VALUES (57, 'admin', 'admin/resource/auth/refresh_node_action', 'admin/resource/auth', 'resource', '刷新权限', '[]', '', '', 3, 1, 1, 1, 0, 1, 0, '2022-12-12 16:29:56', '2022-12-12 16:29:56');
INSERT INTO `qk_system_node` VALUES (58, 'admin', 'admin/resource/group', 'admin', 'resource', '配置组合', '[]', '', '', 2, 1, 1, 1, 0, 1, 0, '2022-12-12 16:29:56', '2022-12-12 16:29:56');
INSERT INTO `qk_system_node` VALUES (59, 'admin', 'admin/resource/group/index', 'admin/resource/group', 'resource', '列表', '[]', '', '', 3, 1, 1, 1, 0, 1, 0, '2022-12-12 16:29:56', '2022-12-12 16:29:56');
INSERT INTO `qk_system_node` VALUES (60, 'admin', 'admin/resource/group/batch_delete_action', 'admin/resource/group', 'resource', '批量删除', '[]', '', '', 3, 1, 1, 1, 1, 1, 0, '2022-12-12 16:29:56', '2022-12-12 16:29:56');
INSERT INTO `qk_system_node` VALUES (61, 'admin', 'admin/resource/group/edit_action', 'admin/resource/group', 'resource', '编辑', '[]', '', '', 3, 1, 1, 1, 1, 1, 0, '2022-12-12 16:29:56', '2022-12-12 16:29:56');
INSERT INTO `qk_system_node` VALUES (62, 'admin', 'admin/resource/group/delete_action', 'admin/resource/group', 'resource', '删除', '[]', '', '', 3, 1, 1, 1, 1, 1, 0, '2022-12-12 16:29:56', '2022-12-12 16:29:56');
INSERT INTO `qk_system_node` VALUES (63, 'admin', 'admin/resource/group/group_data_action', 'admin/resource/group', 'resource', '组合数据', '[]', '', '', 3, 1, 1, 1, 0, 1, 0, '2022-12-12 16:29:56', '2022-12-12 16:29:56');
INSERT INTO `qk_system_node` VALUES (64, 'admin', 'admin/resource/group/add_action', 'admin/resource/group', 'resource', '添加', '[]', '', '', 3, 1, 1, 1, 1, 1, 0, '2022-12-12 16:29:56', '2022-12-12 16:29:56');
INSERT INTO `qk_system_node` VALUES (65, 'admin', 'admin/resource/group_data', 'admin', 'resource', '组合数据', '[]', '', '', 2, 1, 1, 1, 0, 1, 0, '2022-12-12 16:29:56', '2022-12-12 16:29:56');
INSERT INTO `qk_system_node` VALUES (66, 'admin', 'admin/resource/group_data/index', 'admin/resource/group_data', 'resource', '列表', '[]', '', '', 3, 1, 1, 1, 0, 1, 0, '2022-12-12 16:29:56', '2022-12-12 16:29:56');
INSERT INTO `qk_system_node` VALUES (67, 'admin', 'admin/resource/group_data/batch_delete_action', 'admin/resource/group_data', 'resource', '批量删除', '[]', '', '', 3, 1, 1, 1, 1, 1, 0, '2022-12-12 16:29:56', '2022-12-12 16:29:56');
INSERT INTO `qk_system_node` VALUES (68, 'admin', 'admin/resource/group_data/edit_action', 'admin/resource/group_data', 'resource', '编辑', '[]', '', '', 3, 1, 1, 1, 1, 1, 0, '2022-12-12 16:29:56', '2022-12-12 16:29:56');
INSERT INTO `qk_system_node` VALUES (69, 'admin', 'admin/resource/group_data/delete_action', 'admin/resource/group_data', 'resource', '删除', '[]', '', '', 3, 1, 1, 1, 1, 1, 0, '2022-12-12 16:29:56', '2022-12-12 16:29:56');
INSERT INTO `qk_system_node` VALUES (70, 'admin', 'admin/resource/group_data/add_action', 'admin/resource/group_data', 'resource', '添加', '[]', '', '', 3, 1, 1, 1, 1, 1, 0, '2022-12-12 16:29:56', '2022-12-12 16:29:56');
INSERT INTO `qk_system_node` VALUES (71, 'admin', 'admin/resource/menus', 'admin', 'resource', '菜单管理', '[]', '', '', 2, 1, 1, 1, 0, 1, 0, '2022-12-12 16:29:56', '2022-12-12 16:29:56');
INSERT INTO `qk_system_node` VALUES (72, 'admin', 'admin/resource/menus/index', 'admin/resource/menus', 'resource', '列表', '[]', '', '', 3, 1, 1, 1, 0, 1, 0, '2022-12-12 16:29:56', '2022-12-12 16:29:56');
INSERT INTO `qk_system_node` VALUES (73, 'admin', 'admin/resource/menus/inline_input_column_sort', 'admin/resource/menus', 'resource', '排序', '[]', '', '', 3, 0, 1, 1, 1, 1, 0, '2022-12-12 16:29:56', '2022-12-12 16:29:56');
INSERT INTO `qk_system_node` VALUES (74, 'admin', 'admin/resource/menus/switch_column_status', 'admin/resource/menus', 'resource', '启用状态', '[]', '', '', 3, 1, 1, 1, 1, 1, 0, '2022-12-12 16:29:56', '2022-12-12 16:29:56');
INSERT INTO `qk_system_node` VALUES (75, 'admin', 'admin/resource/menus/add_action', 'admin/resource/menus', 'resource', '添加', '[]', '', '', 3, 1, 1, 1, 1, 1, 0, '2022-12-12 16:29:56', '2022-12-12 16:29:56');
INSERT INTO `qk_system_node` VALUES (76, 'admin', 'admin/resource/menus/edit_action', 'admin/resource/menus', 'resource', '编辑', '[]', '', '', 3, 1, 1, 1, 1, 1, 0, '2022-12-12 16:29:56', '2022-12-12 16:29:56');
INSERT INTO `qk_system_node` VALUES (77, 'admin', 'admin/resource/menus/delete_action', 'admin/resource/menus', 'resource', '删除', '[]', '', '', 3, 1, 1, 1, 1, 1, 0, '2022-12-12 16:29:56', '2022-12-12 16:29:56');
INSERT INTO `qk_system_node` VALUES (78, 'admin', 'admin/resource/node', 'admin', 'resource', '节点管理', '[]', '', '', 2, 1, 1, 1, 0, 1, 0, '2022-12-12 16:29:56', '2022-12-12 16:29:56');
INSERT INTO `qk_system_node` VALUES (79, 'admin', 'admin/resource/node/index', 'admin/resource/node', 'resource', '列表', '[]', '', '', 3, 1, 1, 1, 0, 1, 0, '2022-12-12 16:29:56', '2022-12-12 16:29:56');
INSERT INTO `qk_system_node` VALUES (80, 'admin', 'admin/resource/node/batch_delete_action', 'admin/resource/node', 'resource', '批量删除', '[]', '', '', 3, 1, 1, 1, 1, 1, 0, '2022-12-12 16:29:56', '2022-12-12 16:29:56');
INSERT INTO `qk_system_node` VALUES (81, 'admin', 'admin/resource/oplog', 'admin', 'resource', '系统日志', '[]', '', '', 2, 1, 1, 1, 0, 1, 0, '2022-12-12 16:29:56', '2022-12-12 16:29:56');
INSERT INTO `qk_system_node` VALUES (82, 'admin', 'admin/resource/oplog/index', 'admin/resource/oplog', 'resource', '列表', '[]', '', '', 3, 1, 1, 1, 0, 1, 0, '2022-12-12 16:29:56', '2022-12-12 16:29:56');
INSERT INTO `qk_system_node` VALUES (83, 'admin', 'admin/resource/oplog/batch_delete_action', 'admin/resource/oplog', 'resource', '批量删除', '[]', '', '', 3, 1, 1, 1, 1, 1, 0, '2022-12-12 16:29:56', '2022-12-12 16:29:56');
INSERT INTO `qk_system_node` VALUES (84, 'admin', 'admin/resource/oplog/view_action', 'admin/resource/oplog', 'resource', 'View', '[]', '', '', 3, 1, 1, 1, 1, 1, 0, '2022-12-12 16:29:56', '2022-12-12 16:29:56');
INSERT INTO `qk_system_node` VALUES (85, 'admin', 'admin/resource/admin_setting', 'admin', 'resource', '个人中心', '[]', '', '', 2, 1, 1, 1, 0, 1, 0, '2022-12-12 16:32:30', '2022-12-12 16:32:30');
INSERT INTO `qk_system_node` VALUES (86, 'admin', 'admin/resource/admin_setting/index', 'admin/resource/admin_setting', 'resource', '列表', '[]', '', '', 3, 1, 0, 1, 0, 1, 0, '2022-12-12 16:32:30', '2022-12-12 16:32:30');
INSERT INTO `qk_system_node` VALUES (87, 'admin', 'admin/resource/admin_setting/list', 'admin/resource/admin_setting', 'resource', '保存参数', '[]', '', '', 3, 1, 0, 1, 0, 1, 0, '2022-12-12 16:32:30', '2022-12-12 16:32:30');
INSERT INTO `qk_system_node` VALUES (88, 'admin', 'admin/resource/admin_setting/admin_edit_action', 'admin/resource/admin_setting', 'resource', '基本信息', '[]', '', '', 3, 1, 1, 1, 0, 1, 0, '2022-12-12 16:32:30', '2022-12-12 16:32:30');
INSERT INTO `qk_system_node` VALUES (89, 'admin', 'admin/resource/admin_setting/admin_password_action', 'admin/resource/admin_setting', 'resource', '设置密码', '[]', '', '', 3, 1, 1, 1, 0, 1, 0, '2022-12-12 16:32:30', '2022-12-12 16:32:30');
INSERT INTO `qk_system_node` VALUES (90, 'admin', 'admin/resource/config', 'admin', 'resource', '系统配置', '[]', '', '', 2, 1, 1, 1, 0, 1, 0, '2022-12-12 16:32:30', '2022-12-12 16:32:30');
INSERT INTO `qk_system_node` VALUES (91, 'admin', 'admin/resource/config/index', 'admin/resource/config', 'resource', '列表', '[]', '', '', 3, 1, 1, 1, 0, 1, 0, '2022-12-12 16:32:30', '2022-12-12 16:32:30');
INSERT INTO `qk_system_node` VALUES (92, 'admin', 'admin/resource/config/inline_input_column_sort', 'admin/resource/config', 'resource', '排序', '[]', '', '', 3, 0, 1, 1, 1, 1, 0, '2022-12-12 16:32:30', '2022-12-12 16:32:30');
INSERT INTO `qk_system_node` VALUES (93, 'admin', 'admin/resource/config/edit_action', 'admin/resource/config', 'resource', '编辑', '[]', '', '', 3, 1, 1, 1, 1, 1, 0, '2022-12-12 16:32:30', '2022-12-12 16:32:30');
INSERT INTO `qk_system_node` VALUES (94, 'admin', 'admin/resource/config/delete_action', 'admin/resource/config', 'resource', '删除', '[]', '', '', 3, 1, 1, 1, 1, 1, 0, '2022-12-12 16:32:30', '2022-12-12 16:32:30');
INSERT INTO `qk_system_node` VALUES (95, 'admin', 'admin/resource/config/add_action', 'admin/resource/config', 'resource', '添加', '[]', '', '', 3, 1, 1, 1, 1, 1, 0, '2022-12-12 16:32:30', '2022-12-12 16:32:30');
INSERT INTO `qk_system_node` VALUES (96, 'admin', 'admin/resource/config/config_group_add_action', 'admin/resource/config', 'resource', '添加配置分组', '[]', '', '', 3, 0, 1, 1, 0, 1, 0, '2022-12-12 16:32:30', '2022-12-12 16:32:30');
INSERT INTO `qk_system_node` VALUES (97, 'admin', 'admin/resource/config/config_group_edit_action', 'admin/resource/config', 'resource', '编辑配置分组', '[]', '', '', 3, 0, 1, 1, 0, 1, 0, '2022-12-12 16:32:30', '2022-12-12 16:32:30');
INSERT INTO `qk_system_node` VALUES (98, 'admin', 'admin/resource/config/config_group_del_action', 'admin/resource/config', 'resource', '删除配置分组', '[]', '', '', 3, 0, 1, 1, 0, 1, 0, '2022-12-12 16:32:30', '2022-12-12 16:32:30');
INSERT INTO `qk_system_node` VALUES (99, 'admin', 'admin/resource/config/config_group_list_action', 'admin/resource/config', 'resource', '配置分组列表', '[]', '', '', 3, 0, 1, 1, 0, 1, 0, '2022-12-12 16:32:30', '2022-12-12 16:32:30');
INSERT INTO `qk_system_node` VALUES (100, 'admin', 'admin/resource/dashboard', 'admin', 'resource', '系统首页', '[]', '', '', 2, 1, 1, 1, 0, 1, 0, '2022-12-12 16:32:30', '2022-12-12 16:32:30');
INSERT INTO `qk_system_node` VALUES (101, 'admin', 'admin/resource/dashboard/index', 'admin/resource/dashboard', 'resource', '列表', '[]', '', '', 3, 1, 1, 1, 0, 1, 0, '2022-12-12 16:32:30', '2022-12-12 16:32:30');
INSERT INTO `qk_system_node` VALUES (102, 'admin', 'admin/resource/group_new', 'admin', 'resource', '配置组合', '[]', '', '', 2, 1, 1, 1, 0, 1, 0, '2022-12-12 16:32:30', '2022-12-12 16:32:30');
INSERT INTO `qk_system_node` VALUES (103, 'admin', 'admin/resource/group_new/index', 'admin/resource/group_new', 'resource', '列表', '[]', '', '', 3, 1, 1, 1, 0, 1, 0, '2022-12-12 16:32:30', '2022-12-12 16:32:30');
INSERT INTO `qk_system_node` VALUES (104, 'admin', 'admin/resource/group_new/batch_delete_action', 'admin/resource/group_new', 'resource', '批量删除', '[]', '', '', 3, 1, 1, 1, 1, 1, 0, '2022-12-12 16:32:30', '2022-12-12 16:32:30');
INSERT INTO `qk_system_node` VALUES (105, 'admin', 'admin/resource/group_new/switch_column_status', 'admin/resource/group_new', 'resource', '启用状态', '[]', '', '', 3, 1, 1, 1, 1, 1, 0, '2022-12-12 16:32:30', '2022-12-12 16:32:30');
INSERT INTO `qk_system_node` VALUES (106, 'admin', 'admin/resource/group_new/edit_action', 'admin/resource/group_new', 'resource', '编辑', '[]', '', '', 3, 1, 1, 1, 1, 1, 0, '2022-12-12 16:32:30', '2022-12-12 16:32:30');
INSERT INTO `qk_system_node` VALUES (107, 'admin', 'admin/resource/group_new/delete_action', 'admin/resource/group_new', 'resource', '删除', '[]', '', '', 3, 1, 1, 1, 1, 1, 0, '2022-12-12 16:32:30', '2022-12-12 16:32:30');
INSERT INTO `qk_system_node` VALUES (108, 'admin', 'admin/resource/group_new/add_action', 'admin/resource/group_new', 'resource', '添加', '[]', '', '', 3, 1, 1, 1, 1, 1, 0, '2022-12-12 16:32:30', '2022-12-12 16:32:30');
INSERT INTO `qk_system_node` VALUES (109, 'admin', 'admin/resource/group_new/group_add_action', 'admin/resource/group_new', 'resource', '添加数据组', '[]', '', '', 3, 1, 1, 1, 0, 1, 0, '2022-12-12 16:32:30', '2022-12-12 16:32:30');
INSERT INTO `qk_system_node` VALUES (110, 'admin', 'admin/resource/plugin', 'admin', 'resource', '插件管理', '[]', '', '', 2, 1, 1, 1, 0, 1, 0, '2022-12-12 16:32:30', '2022-12-12 16:32:30');
INSERT INTO `qk_system_node` VALUES (111, 'admin', 'admin/resource/plugin/index', 'admin/resource/plugin', 'resource', '列表', '[]', '', '', 3, 1, 1, 1, 0, 1, 0, '2022-12-12 16:32:30', '2022-12-12 16:32:30');
INSERT INTO `qk_system_node` VALUES (112, 'admin', 'admin/resource/plugin/plugin_status_action', 'admin/resource/plugin', 'resource', '插件状态', '[]', '', '', 3, 1, 1, 1, 0, 1, 0, '2022-12-12 16:32:30', '2022-12-12 16:32:30');
INSERT INTO `qk_system_node` VALUES (113, 'admin', 'admin/resource/plugin/plugin_update_action', 'admin/resource/plugin', 'resource', '升级插件', '[]', '', '', 3, 1, 1, 1, 0, 1, 0, '2022-12-12 16:32:30', '2022-12-12 16:32:30');
INSERT INTO `qk_system_node` VALUES (114, 'admin', 'admin/resource/plugin/plugin_install_action', 'admin/resource/plugin', 'resource', '安装插件', '[]', '', '', 3, 1, 1, 1, 0, 1, 0, '2022-12-12 16:32:30', '2022-12-12 16:32:30');
INSERT INTO `qk_system_node` VALUES (115, 'admin', 'admin/resource/plugin/plugin_uninstall_action', 'admin/resource/plugin', 'resource', '卸载插件', '[]', '', '', 3, 1, 1, 1, 0, 1, 0, '2022-12-12 16:32:30', '2022-12-12 16:32:30');
INSERT INTO `qk_system_node` VALUES (116, 'admin', 'admin/resource/plugin/plugin_user_info_action', 'admin/resource/plugin', 'resource', '插件会员信息', '[]', '', '', 3, 1, 1, 1, 0, 1, 0, '2022-12-12 16:32:30', '2022-12-12 16:32:30');
INSERT INTO `qk_system_node` VALUES (117, 'admin', 'admin/resource/system_config', 'admin', 'resource', '系统参数', '[]', '', '', 2, 1, 1, 1, 0, 1, 0, '2022-12-12 16:32:30', '2022-12-12 16:32:30');
INSERT INTO `qk_system_node` VALUES (118, 'admin', 'admin/resource/system_config/list', 'admin/resource/system_config', 'resource', '系统设置', '[]', '', '', 3, 1, 1, 1, 1, 1, 0, '2022-12-12 16:32:30', '2022-12-12 16:32:30');
INSERT INTO `qk_system_node` VALUES (119, 'admin', 'admin/resource/system_config/index', 'admin/resource/system_config', 'resource', '列表', '[]', '', '', 3, 1, 1, 1, 0, 1, 0, '2022-12-12 16:32:30', '2022-12-12 16:32:30');
INSERT INTO `qk_system_node` VALUES (120, 'admin', 'admin/resource/system_queue', 'admin', 'resource', '系统任务', '[]', '', '', 2, 1, 1, 1, 0, 1, 0, '2022-12-12 16:32:30', '2022-12-12 16:32:30');
INSERT INTO `qk_system_node` VALUES (121, 'admin', 'admin/resource/system_queue/index', 'admin/resource/system_queue', 'resource', '列表', '[]', '', '', 3, 1, 1, 1, 0, 1, 0, '2022-12-12 16:32:30', '2022-12-12 16:32:30');
INSERT INTO `qk_system_node` VALUES (122, 'admin', 'admin/resource/system_queue/delete_action', 'admin/resource/system_queue', 'resource', '删除', '[]', '', '', 3, 1, 1, 1, 1, 1, 0, '2022-12-12 16:32:30', '2022-12-12 16:32:30');
INSERT INTO `qk_system_node` VALUES (123, 'admin', 'admin/resource/system_queue/re_queue_action', 'admin/resource/system_queue', 'resource', '重置任务', '[]', '', '', 3, 1, 1, 1, 0, 1, 0, '2022-12-12 16:32:30', '2022-12-12 16:32:30');
INSERT INTO `qk_system_node` VALUES (124, 'admin', 'admin/resource/system_queue/queue_log_action', 'admin/resource/system_queue', 'resource', '任务状态', '[]', '', '', 3, 1, 1, 1, 0, 1, 0, '2022-12-12 16:32:30', '2022-12-12 16:32:30');
INSERT INTO `qk_system_node` VALUES (125, 'admin', 'admin/resource/system_user', 'admin', 'resource', '账户管理', '[]', '', '', 2, 1, 1, 1, 0, 1, 0, '2022-12-12 16:32:30', '2022-12-12 16:32:30');
INSERT INTO `qk_system_node` VALUES (126, 'admin', 'admin/resource/system_user/index', 'admin/resource/system_user', 'resource', '列表', '[]', '', '', 3, 1, 1, 1, 0, 1, 0, '2022-12-12 16:32:30', '2022-12-12 16:32:30');
INSERT INTO `qk_system_node` VALUES (127, 'admin', 'admin/resource/system_user/edit_action', 'admin/resource/system_user', 'resource', '编辑', '[]', '', '', 3, 1, 1, 1, 1, 1, 0, '2022-12-12 16:32:30', '2022-12-12 16:32:30');
INSERT INTO `qk_system_node` VALUES (128, 'admin', 'admin/resource/system_user/add_action', 'admin/resource/system_user', 'resource', '添加', '[]', '', '', 3, 1, 1, 1, 1, 1, 0, '2022-12-12 16:32:30', '2022-12-12 16:32:30');
INSERT INTO `qk_system_node` VALUES (129, 'admin', 'admin/resource/system_user_balance_log', 'admin', 'resource', '余额记录', '[]', '', '', 2, 1, 1, 1, 0, 1, 0, '2022-12-12 16:32:30', '2022-12-12 16:32:30');
INSERT INTO `qk_system_node` VALUES (130, 'admin', 'admin/resource/system_user_balance_log/index', 'admin/resource/system_user_balance_log', 'resource', '列表', '[]', '', '', 3, 1, 1, 1, 0, 1, 0, '2022-12-12 16:32:30', '2022-12-12 16:32:30');
INSERT INTO `qk_system_node` VALUES (131, 'admin', 'admin/resource/system_user_info', 'admin', 'resource', '会员管理', '[]', '', '', 2, 1, 1, 1, 0, 1, 0, '2022-12-12 16:32:30', '2022-12-12 16:32:30');
INSERT INTO `qk_system_node` VALUES (132, 'admin', 'admin/resource/system_user_info/index', 'admin/resource/system_user_info', 'resource', '列表', '[]', '', '', 3, 1, 1, 1, 0, 1, 0, '2022-12-12 16:32:30', '2022-12-12 16:32:30');
INSERT INTO `qk_system_node` VALUES (133, 'admin', 'admin/resource/system_user_info/edit_action', 'admin/resource/system_user_info', 'resource', '编辑', '[]', '', '', 3, 1, 1, 1, 1, 1, 0, '2022-12-12 16:32:30', '2022-12-12 16:32:30');
INSERT INTO `qk_system_node` VALUES (134, 'admin', 'admin/resource/system_user_info/delete_action', 'admin/resource/system_user_info', 'resource', '删除', '[]', '', '', 3, 1, 1, 1, 1, 1, 0, '2022-12-12 16:32:30', '2022-12-12 16:32:30');
INSERT INTO `qk_system_node` VALUES (135, 'admin', 'admin/resource/system_user_info/user_info_integral_action', 'admin/resource/system_user_info', 'resource', '会员积分设置', '[]', '', '', 3, 0, 1, 1, 0, 1, 0, '2022-12-12 16:32:30', '2022-12-12 16:32:30');
INSERT INTO `qk_system_node` VALUES (136, 'admin', 'admin/resource/system_user_info/user_info_balance_action', 'admin/resource/system_user_info', 'resource', '会员余额设置', '[]', '', '', 3, 0, 1, 1, 0, 1, 0, '2022-12-12 16:32:30', '2022-12-12 16:32:30');
INSERT INTO `qk_system_node` VALUES (137, 'admin', 'admin/resource/system_user_info/add_action', 'admin/resource/system_user_info', 'resource', '添加', '[]', '', '', 3, 1, 1, 1, 1, 1, 0, '2022-12-12 16:32:30', '2022-12-12 16:32:30');
INSERT INTO `qk_system_node` VALUES (138, 'admin', 'admin/resource/system_user_integral_log', 'admin', 'resource', '积分记录', '[]', '', '', 2, 1, 1, 1, 0, 1, 0, '2022-12-12 16:32:30', '2022-12-12 16:32:30');
INSERT INTO `qk_system_node` VALUES (139, 'admin', 'admin/resource/system_user_integral_log/index', 'admin/resource/system_user_integral_log', 'resource', '列表', '[]', '', '', 3, 1, 1, 1, 0, 1, 0, '2022-12-12 16:32:30', '2022-12-12 16:32:30');
INSERT INTO `qk_system_node` VALUES (140, 'admin', 'admin/resource/upgrade', 'admin', 'resource', '升级管理', '[]', '', '', 2, 1, 1, 1, 0, 1, 0, '2022-12-12 16:32:30', '2022-12-12 16:32:30');
INSERT INTO `qk_system_node` VALUES (141, 'admin', 'admin/resource/upgrade/index', 'admin/resource/upgrade', 'resource', '列表', '[]', '', '', 3, 1, 1, 1, 0, 1, 0, '2022-12-12 16:32:30', '2022-12-12 16:32:30');
INSERT INTO `qk_system_node` VALUES (142, 'admin', 'admin/resource/upgrade/upgrade_queue_action', 'admin/resource/upgrade', 'resource', '更新系统', '[]', '', '', 3, 1, 1, 1, 0, 1, 0, '2022-12-12 16:32:30', '2022-12-12 16:32:30');
INSERT INTO `qk_system_node` VALUES (143, 'crud', 'crud/resource/index', 'crud', 'resource', '首页', '[]', '', '', 2, 1, 1, 1, 0, 1, 0, '2022-12-12 16:32:30', '2022-12-12 16:32:30');
INSERT INTO `qk_system_node` VALUES (144, 'crud', 'crud/resource/index/index', 'crud/resource/index', 'resource', '数据列表', '[]', '', '', 3, 0, 0, 0, 0, 1, 0, '2022-12-12 16:32:30', '2022-12-12 16:32:30');

-- ----------------------------
-- Table structure for qk_system_oplog
-- ----------------------------
DROP TABLE IF EXISTS `qk_system_oplog`;
CREATE TABLE `qk_system_oplog`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `type` tinyint(1) NULL DEFAULT 0 COMMENT '日志类型:0=后台管理,1=用户接口',
  `plugin` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '模块',
  `node` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '当前操作节点',
  `geoip` varchar(15) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '操作者IP地址',
  `action` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '操作行为名称',
  `user_id` int(11) NOT NULL DEFAULT 0 COMMENT '账户id',
  `user_agent` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT 'User-Agent',
  `content` varchar(1024) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '操作内容描述',
  `username` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '操作人用户名',
  `params`  longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL  COMMENT '请求参数',
  `method` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '请求方法',
  `json_result` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '放回数据',
  `error_msg` varchar(4000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '错误信息',
  `created_at` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建日期',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 161 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '系统-日志' ROW_FORMAT = Dynamic;


-- ----------------------------
-- Table structure for qk_system_plugin
-- ----------------------------
DROP TABLE IF EXISTS `qk_system_plugin`;
CREATE TABLE `qk_system_plugin`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '自增id',
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '0' COMMENT '插件key',
  `display_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '显示名称',
  `desc` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '0' COMMENT '描述',
  `avatar` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '图标',
  `version` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '版本号',
  `status` tinyint(1) NOT NULL DEFAULT 1 COMMENT '状态 1:启用, 0:禁用',
  `is_deleted` tinyint(1) NOT NULL DEFAULT 0 COMMENT '删除: 1已删除 0未删除',
  `deleted_at` int(11) NOT NULL DEFAULT 0 COMMENT '删除日期',
  `create_by` int(11) NOT NULL DEFAULT 0 COMMENT '创建人admin_id',
  `created_at` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建日期',
  `updated_at` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新日期',
  `update_by` int(11) NOT NULL DEFAULT 0 COMMENT '修改人admin_id',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_is_deleted`(`is_deleted`) USING BTREE,
  INDEX `idx_status`(`status`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '系统插件' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of qk_system_plugin
-- ----------------------------
INSERT INTO `qk_system_plugin` VALUES (3, 'crud', 'crud', '快捷生成代码', '', '', 1, 0, 2147483647, 0, '2022-02-25 11:30:06', '2022-02-25 11:30:06', 0);

-- ----------------------------
-- Table structure for qk_system_queue
-- ----------------------------
DROP TABLE IF EXISTS `qk_system_queue`;
CREATE TABLE `qk_system_queue`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `code` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '任务编号',
  `title` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '任务名称',
  `queue` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '队列名称',
  `payload` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '有效负载',
  `command` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '执行指令',
  `run_pid` bigint(20) NULL DEFAULT 0 COMMENT '执行进程',
  `available_time` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '有效时间',
  `reserve_time` int(10) UNSIGNED NULL DEFAULT 0 COMMENT '执行时间',
  `enter_time` decimal(20, 4) NULL DEFAULT 0.0000 COMMENT '开始时间',
  `outer_time` decimal(20, 4) NULL DEFAULT 0.0000 COMMENT '结束时间',
  `desc` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '执行描述',
  `attempts_max` int(10) NOT NULL DEFAULT 3 COMMENT '重发最大次数',
  `loops_time` bigint(20) NULL DEFAULT 0 COMMENT '循环时间',
  `attempts` bigint(20) NULL DEFAULT 0 COMMENT '执行次数',
  `rscript` tinyint(1) NULL DEFAULT 1 COMMENT '任务类型(0单例,1多例)',
  `status` tinyint(1) NULL DEFAULT 1 COMMENT '任务状态(1新任务,2处理中,3成功,4失败)',
  `create_time` int(10) NOT NULL DEFAULT 0 COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_system_queue_code`(`code`) USING BTREE,
  INDEX `idx_system_queue_title`(`title`) USING BTREE,
  INDEX `idx_system_queue_status`(`status`) USING BTREE,
  INDEX `idx_system_queue_rscript`(`rscript`) USING BTREE,
  INDEX `idx_system_queue_create_at`(`create_time`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 74 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '系统-任务' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of qk_system_queue
-- ----------------------------
INSERT INTO `qk_system_queue` VALUES (64, 'Q202212036513862', '系统更新', 'quick', '{\"job\":\"app\\\\admin\\\\jobs\\\\UpgradeJob\",\"maxTries\":null,\"timeout\":null,\"data\":{\"do\":\"handle\",\"attempts_max\":0,\"title\":\"\\u7cfb\\u7edf\\u66f4\\u65b0\",\"release\":0,\"rscript\":0,\"code\":\"Q202212036513862\",\"data\":[]}}', 'app\\admin\\jobs\\UpgradeJob', 0, 1670075053, 1670075057, 1670075057.1065, 1670075092.1599, NULL, 0, 0, 1, 0, 3, 1670075053);
INSERT INTO `qk_system_queue` VALUES (65, 'Q202212036639235', '系统更新', 'quick', '{\"job\":\"app\\\\admin\\\\jobs\\\\UpgradeJob\",\"maxTries\":null,\"timeout\":null,\"data\":{\"do\":\"handle\",\"attempts_max\":0,\"title\":\"\\u7cfb\\u7edf\\u66f4\\u65b0\",\"release\":0,\"rscript\":0,\"code\":\"Q202212036639235\",\"data\":[]}}', 'app\\admin\\jobs\\UpgradeJob', 0, 1670075139, 1670075142, 1670075142.1078, 1670075147.1453, NULL, 0, 0, 1, 0, 3, 1670075139);
INSERT INTO `qk_system_queue` VALUES (66, 'Q202212056214676', '系统更新', 'quick', '{\"job\":\"app\\\\admin\\\\jobs\\\\UpgradeJob\",\"maxTries\":null,\"timeout\":null,\"data\":{\"do\":\"handle\",\"attempts_max\":0,\"title\":\"\\u7cfb\\u7edf\\u66f4\\u65b0\",\"release\":0,\"rscript\":0,\"code\":\"Q202212056214676\",\"data\":[]}}', 'app\\admin\\jobs\\UpgradeJob', 0, 1670222894, 1670222896, 1670222896.6606, 1670222911.3342, NULL, 0, 0, 1, 0, 3, 1670222894);
INSERT INTO `qk_system_queue` VALUES (67, 'Q202212064605210', '系统更新', 'quick', '{\"job\":\"app\\\\admin\\\\jobs\\\\UpgradeJob\",\"maxTries\":null,\"timeout\":null,\"data\":{\"do\":\"handle\",\"attempts_max\":0,\"title\":\"\\u7cfb\\u7edf\\u66f4\\u65b0\",\"release\":0,\"rscript\":0,\"code\":\"Q202212064605210\",\"data\":[]}}', 'app\\admin\\jobs\\UpgradeJob', 0, 1670315405, 1670315406, 1670315406.1541, 1670315419.8434, NULL, 0, 0, 1, 0, 3, 1670315405);
INSERT INTO `qk_system_queue` VALUES (68, 'Q202212071536864', '系统更新', 'quick', '{\"job\":\"app\\\\admin\\\\jobs\\\\UpgradeJob\",\"maxTries\":null,\"timeout\":null,\"data\":{\"do\":\"handle\",\"attempts_max\":0,\"title\":\"\\u7cfb\\u7edf\\u66f4\\u65b0\",\"release\":0,\"rscript\":0,\"code\":\"Q202212071536864\",\"data\":[]}}', 'app\\admin\\jobs\\UpgradeJob', 0, 1670392896, 1670392897, 1670392897.7731, 1670392908.5924, NULL, 0, 0, 1, 0, 3, 1670392896);
INSERT INTO `qk_system_queue` VALUES (69, 'Q202212123038018', '系统更新', 'quick', '{\"job\":\"app\\\\admin\\\\jobs\\\\UpgradeJob\",\"maxTries\":null,\"timeout\":null,\"data\":{\"do\":\"handle\",\"attempts_max\":0,\"title\":\"\\u7cfb\\u7edf\\u66f4\\u65b0\",\"release\":0,\"rscript\":0,\"code\":\"Q202212123038018\",\"data\":[]}}', 'app\\admin\\jobs\\UpgradeJob', 0, 1670832879, 1670832880, 1670832880.8097, 1670832931.9862, NULL, 0, 0, 1, 0, 3, 1670832880);
INSERT INTO `qk_system_queue` VALUES (70, 'Q202212125040455', '系统更新', 'quick', '{\"job\":\"app\\\\admin\\\\jobs\\\\UpgradeJob\",\"maxTries\":null,\"timeout\":null,\"data\":{\"do\":\"handle\",\"attempts_max\":0,\"title\":\"\\u7cfb\\u7edf\\u66f4\\u65b0\",\"release\":0,\"rscript\":0,\"code\":\"Q202212125040455\",\"data\":[]}}', 'app\\admin\\jobs\\UpgradeJob', 0, 1670834081, 1670834081, 1670834081.6962, 1670834099.7194, NULL, 0, 0, 1, 0, 3, 1670834081);
INSERT INTO `qk_system_queue` VALUES (71, 'Q202212125148939', '系统更新', 'quick', '{\"job\":\"app\\\\admin\\\\jobs\\\\UpgradeJob\",\"maxTries\":null,\"timeout\":null,\"data\":{\"do\":\"handle\",\"attempts_max\":0,\"title\":\"\\u7cfb\\u7edf\\u66f4\\u65b0\",\"release\":0,\"rscript\":0,\"code\":\"Q202212125148939\",\"data\":[]}}', 'app\\admin\\jobs\\UpgradeJob', 0, 1670834148, 1670834151, 1670834151.4343, 1670834155.4416, NULL, 0, 0, 1, 0, 3, 1670834148);
INSERT INTO `qk_system_queue` VALUES (72, 'Q202212125651876', '系统更新', 'quick', '{\"job\":\"app\\\\admin\\\\jobs\\\\UpgradeJob\",\"maxTries\":null,\"timeout\":null,\"data\":{\"do\":\"handle\",\"attempts_max\":0,\"title\":\"\\u7cfb\\u7edf\\u66f4\\u65b0\",\"release\":0,\"rscript\":0,\"code\":\"Q202212125651876\",\"data\":[]}}', 'app\\admin\\jobs\\UpgradeJob', 0, 1670834451, 1670834451, 1670834451.1763, 1670834453.0607, NULL, 0, 0, 1, 0, 3, 1670834451);
INSERT INTO `qk_system_queue` VALUES (73, 'Q202212126311879', '系统更新', 'quick', '{\"job\":\"app\\\\admin\\\\jobs\\\\UpgradeJob\",\"maxTries\":null,\"timeout\":null,\"data\":{\"do\":\"handle\",\"attempts_max\":0,\"title\":\"\\u7cfb\\u7edf\\u66f4\\u65b0\",\"release\":0,\"rscript\":0,\"code\":\"Q202212126311879\",\"data\":[]}}', 'app\\admin\\jobs\\UpgradeJob', 0, 1670834831, 1670834834, 1670834834.0952, 1670834835.9386, NULL, 0, 0, 1, 0, 3, 1670834831);

-- ----------------------------
-- Table structure for qk_system_user
-- ----------------------------
DROP TABLE IF EXISTS `qk_system_user`;
CREATE TABLE `qk_system_user`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '账号id',
  `username` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '账户名称',
  `nickname` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '昵称',
  `avatar` varchar(150) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '头像',
  `email` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '电子邮箱',
  `phone` varchar(15) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '手机号',
  `password` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '密码',
  `salt` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '密码盐',
  `status` tinyint(1) NOT NULL DEFAULT 0 COMMENT '状态 1:启用, 0:禁用',
  `last_login_ip_at` varchar(25) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '最后一次登录ip',
  `create_ip_at` varchar(25) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '创建ip',
  `login_num` int(11) NOT NULL DEFAULT 0 COMMENT '登录次数',
  `login_fail_num` tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '失败次数',
  `is_deleted` tinyint(1) NOT NULL DEFAULT 0 COMMENT '删除: 1已删除 0未删除',
  `login_at` timestamp(0) NULL DEFAULT NULL,
  `deleted_at` int(11) NOT NULL DEFAULT 0 COMMENT '删除日期',
  `created_at` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建日期',
  `updated_at` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新日期',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_email`(`email`) USING BTREE,
  INDEX `idx_phone`(`phone`) USING BTREE,
  INDEX `idx_username`(`username`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 47 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '系统统一账户' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of qk_system_user
-- ----------------------------
-- INSERT INTO `qk_system_user` VALUES (45, 'admin', 'admin', '', '', '', '333ead5b69792d0b7feb64d965edb788', 'r4c6', 1, '', '', 0, 0, 0, '2022-12-12 16:31:08', 2147483647, '2022-07-20 14:59:47', '2022-12-12 16:31:09');

-- ----------------------------
-- Table structure for qk_system_user_balance_log
-- ----------------------------
DROP TABLE IF EXISTS `qk_system_user_balance_log`;
CREATE TABLE `qk_system_user_balance_log`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `type` tinyint(1) NOT NULL COMMENT '类型:1=收入,2=支出',
  `num` decimal(10, 2) NOT NULL COMMENT '变动数量',
  `current_num` decimal(10, 2) NOT NULL DEFAULT 0.00 COMMENT '当前余额-变动后',
  `desc` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '变动说明',
  `full_desc` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '自定义详细说明|记录',
  `sign` varchar(155) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '关联订单标识',
  `order_no` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '订单号',
  `created_at` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建日期',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_id`(`user_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '余额记录' ROW_FORMAT = Dynamic;



-- ----------------------------
-- Table structure for qk_system_user_info
-- ----------------------------
DROP TABLE IF EXISTS `qk_system_user_info`;
CREATE TABLE `qk_system_user_info`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '自增id',
  `user_id` int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '账号id',
  `gender` enum('male','female','unknow') CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT 'unknow' COMMENT '性别',
  `integral` int(11) NOT NULL DEFAULT 0 COMMENT '积分',
  `total_integral` int(11) NOT NULL DEFAULT 0 COMMENT '最高积分',
  `balance` decimal(10, 2) NOT NULL DEFAULT 0.00 COMMENT '余额',
  `total_balance` decimal(10, 2) NOT NULL DEFAULT 0.00 COMMENT '总余额',
  `contact_way` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '联系方式',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '备注',
  `motto` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '个性签名',
  `parent_id` int(11) NOT NULL DEFAULT 0 COMMENT '上级id',
  `temp_parent_id` int(11) NOT NULL DEFAULT 0 COMMENT '临时上级',
  `platform_openid` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '平台id 如微信 openid',
  `platform` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '用户所属平台标识 facebook,google,wechat,qq,weibo,twitter,weapp',
  `is_deleted` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_id`(`user_id`) USING BTREE,
  INDEX `idx_parent_id`(`parent_id`) USING BTREE,
  INDEX `idx_temp_parent_id`(`temp_parent_id`) USING BTREE,
  INDEX `idx_platform_id`(`platform_openid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '用户信息' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for qk_system_user_integral_log
-- ----------------------------
DROP TABLE IF EXISTS `qk_system_user_integral_log`;
CREATE TABLE `qk_system_user_integral_log`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `type` tinyint(1) NOT NULL COMMENT '类型:1=收入,2=支出',
  `integral` int(11) NOT NULL COMMENT '变动积分',
  `current_integral` int(11) NOT NULL DEFAULT 0 COMMENT '当前积分-变动后',
  `desc` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '变动说明',
  `full_desc` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '自定义详细说明|记录',
  `sign` varchar(155) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '关联订单标识',
  `order_no` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '订单号',
  `created_at` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建日期',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_id`(`user_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '积分记录' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for qk_system_user_platform
-- ----------------------------
DROP TABLE IF EXISTS `qk_system_user_platform`;
CREATE TABLE `qk_system_user_platform`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '自增id',
  `user_id` int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '账号id',
  `nickname` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '昵称',
  `avatar` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '头像',
  `gender` enum('male','female','unknow') CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT 'unknow' COMMENT '性别',
  `platform_openid` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '平台id 如微信 openid',
  `platform` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '用户所属平台标识 facebook,google,wechat,qq,weibo,twitter,weapp',
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT 'h5密码',
  `unionid` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '微信unionid',
  `subscribe` tinyint(1) NOT NULL DEFAULT 0 COMMENT '微信是否关注',
  `created_at` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建日期',
  `updated_at` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新日期',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_platform`(`platform`) USING BTREE,
  INDEX `idx_platform_id`(`platform_openid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '第三方用户信息' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for qk_system_user_token
-- ----------------------------
DROP TABLE IF EXISTS `qk_system_user_token`;
CREATE TABLE `qk_system_user_token`  (
  `token` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Token',
  `user_id` int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '会员ID',
  `create_time` int(11) NULL DEFAULT NULL COMMENT '创建时间',
  `expire_time` int(11) NULL DEFAULT NULL COMMENT '过期时间',
  INDEX `idx_token`(`token`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '会员Token表' ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;
