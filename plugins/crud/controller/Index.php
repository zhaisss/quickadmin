<?php

declare(strict_types=1);

namespace plugins\crud\controller;



use app\common\controller\Backend;
use app\common\model\SystemMenu;
use app\Request;
use plugins\crud\command\service\BuildResource;
use plugins\crud\command\service\BuildTable;
use plugins\crud\command\service\CrudConfig;
use plugins\crud\command\service\CrudGenerator;
use plugins\crud\command\service\ModelsGenerator;
use plugins\crud\command\service\ParseModel;
use plugins\crud\command\service\BuildForm;
use plugins\crud\command\service\ServiceGenerator;
use quick\admin\annotation\AdminAuth;
use quick\admin\components\Component;
use quick\admin\library\tools\TreeArray;
use think\facade\Cache;
use think\facade\Db;
use think\helper\Str;

/**
 * Class Index
 * @AdminAuth(
 *     title="crud",
 *     auth=true,
 *     menu=true,
 *     login=false
 * )
 * @package app\admin\controller
 */
class Index extends Backend
{


    /**
     * @AdminAuth(title="获取表字段",auth=true,menu=true,login=true,log=false)
     * @param Request $request
     * @return \think\response\Json
     */
    public function tableFields(Request $request)
    {
        $table = $request->post('table');
        $isRelation = $request->post('relation');
        $data = ParseModel::getTableFields($table);
        $data = array_values($data);
        $prefix = Db::table($table)->newQuery()->getConfig('prefix');
        $relationName = stripos($table, $prefix) === 0 ? substr($table, strlen($prefix)) : $table;

        foreach ($data as &$item){
            if($isRelation){
                $item['relation'] = $relationName;
            }else{
                $item['relation'] = false;
            }

            $item['rule'] = implode("|",$item['rule']);
        }

        $config = CrudConfig::formItem();
        $resourceName = Str::studly($relationName);
        return json([
            'code' => 0,
            'data' => [
                'type' => $config,
                'info' => [
                    'resourceName' => $resourceName,
                    'menuName' => $resourceName,
                    'model' => $resourceName,
                ],
                'list' => $data,
            ],
        ]);
    }


    /**
     * @AdminAuth(title="获取表列表",auth=true,menu=true,login=true,log=false)
     * @return \think\response\Json
     */
    public function tableList()
    {

        $list = [];
        $data = Db::query("SHOW TABLES");
        foreach ($data as $key => $row) {
            $list[reset($row)] = reset($row);
        }
        return json([
            'code' => 0,
            'data' => $list,
        ]);
    }


    /**
     * @AdminAuth(title="预览表单",auth=true,menu=true,login=true,log=false)
     * @param Request $request
     * @return \think\response\Json
     * @throws \Exception
     */
    public function previewForm(Request $request)
    {

        $fields = json_decode($request->post('fields',[]),true);
        $form = BuildForm::buildField($fields,'');
        return json([
            'code' => 0,
            'data' => $form,
            'fields' => $fields,
        ]);
    }


    /**
     * @AdminAuth(title="预览Model",auth=true,menu=true,login=true,log=false)
     * @param Request $request
     * @return \think\response\Json
     * @throws \Exception
     */
    public function previewModel(Request $request)
    {
        $fields = json_decode($request->post('fields',[]),true);
        $name = $request->post('name');
        $namespace = $request->post('namespace');
        $service = new ModelsGenerator();
        $service->name = $name;
        $service->namespace = $namespace;
        $service->table = $request->post('table');
        $service->fields = $fields;
        $service->relations = json_decode($request->post('relation',[]),true);
        if($request->post('show_type') == 'lang'){
            $res = $service->createLang();
        }else{
            $res = $service->create();
        }

        return json([
            'code' => 0,
            'data' => Component::custom('qk-code')->props('data', $res),
        ]);
    }


    /**
     * @AdminAuth(title="预览Table",auth=true,menu=true,login=true,log=false)
     * @param Request $request
     * @return \think\response\Json
     * @throws \Exception
     */
    public function previewTable(Request $request)
    {

        $fields = json_decode($request->post('fields',[]),true);
        $form = BuildTable::buildField($fields,'');
        return json([
            'code' => 0,
            'data' => $form,
            'fields' => $fields,
        ]);
    }


    /**
     * @AdminAuth(title="预览Resource",auth=true,menu=true,login=true,log=false)
     * @param Request $request
     * @return \think\response\Json
     * @throws \Exception
     */
    public function previewResource(Request $request)
    {

        $name = $request->post('name');
        $namespace = $request->post('namespace');
        $service = new ModelsGenerator();
        $service->name = $name;
        $service->namespace = $namespace;
        $service->table = $request->post('table');

        $fields = json_decode($request->post('fields',[]),true);
        $form = new BuildResource();
        $form->name =  $request->post('resource_name','');
        $form->namespace =  $request->post('resource_namespace','');
        $form->table =  $request->post('table','');
        $form->modelClass =  $service->getClassName($service->table);
        $form->modelName =  $service->getModelName($service->table);
        $res = $form->create($fields);
        return json([
            'code' => 0,
            'data' =>  Component::custom('qk-code')->props('data', $res),
        ]);
    }


    /**
     * @AdminAuth(title="创建CRUD",auth=true,menu=true,login=true,log=true)
     * @param Request $request
     * @return \think\response\Json
     */
    public function createCrud(Request $request)
    {
        $service = new CrudGenerator();
        $service->name = $request->post('name');;
        $service->create_model = $request->post('create_model');
        $service->create_resource = $request->post('create_resource');
        $service->create_lang = $request->post('create_lang');
        $service->namespace =  $request->post('namespace','');
        $service->resource_namespace =  $request->post('resource_namespace','');
        $service->resource_name =  $request->post('resource_name','');
        $service->table = $request->post('table');
        $service->relations = json_decode($request->post('relations',[]),true);
        $service->fields = json_decode($request->post('fields',[]),true);
        $service->force = $request->post('force',false);
        $service->create_menu = $request->post('create_menu',false);
        $service->menuName = $request->post('menuName','');
        $service->menuPid = $request->post('menuPid',0);

        $res = $service->create();
        return json([
            'code' => 0,
            'data' => Component::custom('qk-code')->props('data', $res),
        ]);
    }


    /**
     * @AdminAuth(title="预览Servcie",auth=true,menu=true,login=true,log=false)
     * @param Request $request
     * @return \think\response\Json
     */
    public function previewAction(Request $request)
    {
        $fields = $request->post('fields');
        $name = $request->post('name');
        $namespace = $request->post('namespace');

        try {
            $service = new ServiceGenerator();
            $service->fields = $fields;
            $service->name = $name;
            $service->namespace = $namespace;
            $res = $service->create(false);
        }catch (\Exception $e){
            return json([
                'code' => 1,
                'msg' => $e->getMessage(),
            ]);
        }

        return json([
            'code' => 0,
            'data' => Component::custom('qk-code')->props('data', $res),
        ]);

    }


    /**
     * @AdminAuth(title="创建Servcie",auth=true,menu=true,login=true,log=true)
     * @param Request $request
     * @return \think\response\Json
     */
    public function createAction(Request $request)
    {
        $fields = $request->post('fields');
        $name = $request->post('name');
        $namespace = $request->post('namespace');
        $force = $request->post('force');

        try {
            $service = new ServiceGenerator();
            $service->fields = $fields;
            $service->name = $name;
            $service->namespace = $namespace;
            $service->force = $force;
            $service->create(true);
            Cache::clear();// 清除缓存
        }catch (\Exception $e){
            return json([
                'code' => 1,
                'msg' => $e->getMessage(),
            ]);
        }

        return json([
            'code' => 0,
            'msg' => '创建成功',
        ]);

    }


    /**
     * @AdminAuth(title="创建Api",auth=true,menu=true,login=true,log=true)
     * @param Request $request
     * @return \think\response\Json
     */
    public function createApi(Request $request)
    {
        $fields = $request->post('fields');
        $name = $request->post('name');
        $namespace = $request->post('namespace');
        $force = $request->post('force');

        try {
            $service = new ServiceGenerator();
            $service->fields = $fields;
            $service->name = $name;
            $service->namespace = $namespace;
            $service->force = $force;
            $service->create(true);
        }catch (\Exception $e){
            return json([
                'code' => 1,
                'msg' => $e->getMessage(),
            ]);
        }

        return json([
            'code' => 0,
            'msg' => '创建成功',
        ]);

    }


    /**
     * @AdminAuth(title="菜单",auth=true,menu=true,login=true,log=false)
     * @param Request $request
     * @return \think\response\Json
     */
    public function menuList()
    {
        $menus = SystemMenu::where([
            "status" => 1,
            "plugin_name" => 'admin',
        ])->select()->toArray();
        array_unshift($menus, ['id' => 0, 'title' => "顶级菜单", 'pid' => "-1"]);
        $menus = TreeArray::arr2table($menus, "id", "pid", "url_key");
        foreach ($menus as &$menu) {
            $menu['title'] = $menu['spl'] . $menu['title'];
        }
        return json([
            'code' => 0,
            'data' => $menus,
            'msg' => 'success',
        ]);
    }
}
