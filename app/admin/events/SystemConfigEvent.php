<?php

namespace app\admin\events;

use app\common\events\BaseEvent;
use app\common\model\SystemConfigGroup;
use quick\admin\form\Form;

class SystemConfigEvent extends BaseEvent
{

    protected array $configList = [];


    protected string $tabKey = 'group';


    public function getConfigList()
    {
        return $this->configList;

    }

    /**
     * 添加配置
     * @param string $tabKey
     * @param string $name
     * @param \Closure $func
     * @return $this
     */
    public function addTab(string $tabKey,string $name,\Closure $func)
    {
        $form = Form::make();
        call_user_func($func,$form);
        $config = [
            'key' => $tabKey,
            'title' => $name,
            'form' => $form,
            'child' => [],
        ];

        $topKey = $tabKey;
        $keys = explode('.',$tabKey);
        if(count($keys) > 1){
            $topKey = $keys[0];
            $twoKey = $keys[1];
            $configItem = [
                'key' => $twoKey,
                'title' => $name,
                'form' => $form,
                'child' => [],
            ];
            if(empty($this->configList[$topKey])){
                $config = [
                    'key' => $topKey,
                    'title' => $name,
                    'form' => [],
                    'child' => [
                        $twoKey =>  $configItem
                    ],
                ];
                $this->configList[$topKey] = $config;
            }else{
                $this->configList[$topKey]['child'][$twoKey] = $configItem;
            }
            return $this;
        }
        $this->configList[$topKey] = $config;
        return $this;
    }

    public function getTopTabs()
    {

    }

    public function getContent()
    {

    }
}