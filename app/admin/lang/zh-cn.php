<?php

return [
    'admin'                                  => '系统管理',
    'Your verification code is: %s'          => '您的验证码是：%s，十分钟内有效~',
    'Member registration verification'       => '会员注册验证',
    'Mail sent successfully~'                => '邮件发送成功~',
    'Unknown operation'                      => '未知操作~',
];
