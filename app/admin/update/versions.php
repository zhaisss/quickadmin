<?php
/**
 * 版本升级文件
 */
return [
    '1.0.1' => function () {
        $sql = <<<EOF
ALTER TABLE `qk_system_admin_info` ADD COLUMN `is_super_admin` tinyint(1) NOT NULL DEFAULT 0 COMMENT '超级管理员' AFTER `status`;
ALTER TABLE `qk_system_attachment` MODIFY COLUMN `deleted_at` int(11) NOT NULL DEFAULT 0 COMMENT '删除日期' AFTER `is_deleted`;
ALTER TABLE `qk_system_attachment_cate` MODIFY COLUMN `deleted_at` int(11) NOT NULL DEFAULT 0 COMMENT '删除日期' AFTER `is_deleted`;
ALTER TABLE `qk_system_attachment_storage` MODIFY COLUMN `deleted_at` int(11) NOT NULL DEFAULT 0 COMMENT '删除日期' AFTER `is_deleted`;
ALTER TABLE `qk_system_auth` MODIFY COLUMN `deleted_at` int(11) NOT NULL DEFAULT 0 COMMENT '删除日期' AFTER `status`;
ALTER TABLE `qk_system_node` ADD COLUMN `method` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '请求类型json' AFTER `title`;
ALTER TABLE `qk_system_node` ADD COLUMN `is_log` tinyint(1) NULL DEFAULT 0 COMMENT '记录日志' AFTER `is_login`;
ALTER TABLE `qk_system_node` MODIFY COLUMN `plugin_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '系统插件plugin' AFTER `id`;
ALTER TABLE `qk_system_node` MODIFY COLUMN `mode` varchar(150) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '节点类型:controller、resource' AFTER `pnode`;
ALTER TABLE `qk_system_oplog` ADD COLUMN `type` tinyint(1) NULL DEFAULT 0 COMMENT '日志类型:0=后台管理,1=用户接口' AFTER `id`;
ALTER TABLE `qk_system_oplog` ADD COLUMN `plugin` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '模块' AFTER `type`;
ALTER TABLE `qk_system_oplog` ADD COLUMN `user_id` int(11) NOT NULL DEFAULT 0 COMMENT '账户id' AFTER `action`;
ALTER TABLE `qk_system_oplog` ADD COLUMN `user_agent` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT 'User-Agent' AFTER `user_id`;
ALTER TABLE `qk_system_oplog` ADD COLUMN `params` varchar(4000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '请求参数' AFTER `username`;
ALTER TABLE `qk_system_oplog` ADD COLUMN `method` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '请求方法' AFTER `params`;
ALTER TABLE `qk_system_oplog` ADD COLUMN `json_result` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '放回数据' AFTER `method`;
ALTER TABLE `qk_system_oplog` ADD COLUMN `error_msg` varchar(4000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '错误信息' AFTER `json_result`;
ALTER TABLE `qk_system_oplog` MODIFY COLUMN `username` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '操作人用户名' AFTER `content`;
ALTER TABLE `qk_system_plugin` MODIFY COLUMN `deleted_at` int(11) NOT NULL DEFAULT 0 COMMENT '删除日期' AFTER `is_deleted`;
ALTER TABLE `qk_system_user` MODIFY COLUMN `deleted_at` int(11) NOT NULL DEFAULT 0 COMMENT '删除日期' AFTER `login_at`;
CREATE TABLE `qk_system_user_balance_log`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `type` tinyint(1) NOT NULL COMMENT '类型:1=收入,2=支出',
  `num` decimal(10, 2) NOT NULL COMMENT '变动数量',
  `current_num` decimal(10, 2) NOT NULL DEFAULT 0.00 COMMENT '当前余额-变动后',
  `desc` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '变动说明',
  `full_desc` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '自定义详细说明|记录',
  `sign` varchar(155) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '关联订单标识',
  `order_no` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '订单号',
  `created_at` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建日期',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_id`(`user_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '余额记录' ROW_FORMAT = Dynamic;
ALTER TABLE `qk_system_user_info` ADD COLUMN `motto` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '个性签名' AFTER `remark`;
CREATE TABLE `qk_system_user_integral_log`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `type` tinyint(1) NOT NULL COMMENT '类型:1=收入,2=支出',
  `integral` int(11) NOT NULL COMMENT '变动积分',
  `current_integral` int(11) NOT NULL DEFAULT 0 COMMENT '当前积分-变动后',
  `desc` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '变动说明',
  `full_desc` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '自定义详细说明|记录',
  `sign` varchar(155) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '关联订单标识',
  `order_no` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '订单号',
  `created_at` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建日期',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_id`(`user_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '积分记录' ROW_FORMAT = Dynamic;

EOF;
      executeSql($sql);
    },
    '1.0.2' => function () {
        $sql = <<<EOF
        ALTER TABLE `qk_system_admin_info` MODIFY COLUMN `auth_set` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '权限' AFTER `plugin_name`;
        ALTER TABLE `qk_system_auth` DROP COLUMN `node_set`;
        ALTER TABLE `qk_system_oplog` MODIFY COLUMN `params` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '请求参数' AFTER `username`;
EOF;
        executeSql($sql);
    }
];