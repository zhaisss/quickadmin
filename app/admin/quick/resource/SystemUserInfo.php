<?php
declare (strict_types = 1);
namespace app\admin\quick\resource;

use app\admin\quick\actions\UserInfoBalanceAction;
use app\admin\quick\actions\UserInfoIntegralAction;
use quick\admin\annotation\AdminAuth;
use quick\admin\components\Component;
use quick\admin\filter\Filter;
use quick\admin\form\Form;
use quick\admin\Resource;
use quick\admin\table\Query;
use quick\admin\table\Table;
use think\Request;


/**
 *
 * Class SystemUserInfo
 * @AdminAuth(title="会员管理",auth=true,menu=true,login=true )
 * @package app\admin\quick\resource
 */
class SystemUserInfo extends Resource
{
    /**
     * 标题
     *
     * @var string
     */
    protected $title = 'title';

    /**
     * @var string
     */
    protected $description = "desc";

    /**
     * 关联模型 app\admin\model\SystemUserInfo
     *
     * @var string
     */
    protected static $model = 'app\common\model\SystemUserInfo';

    /**
     * 可搜索字段
     *
     * @var array
     */
    public static $search = [
        'user.username',
        'user.nickname',
        'user.email',
        'user.phone',
    ];

    protected string $searchPlaceholder = '输入用户名、昵称、手机号、邮箱搜索';

    protected function model(Query $model)
    {
        $model->withJoin('user');
        return $model;
    }


    /**
     * @param Filter $filter
     * @return false
     */
    protected function filters(Filter $filter)
    {
        $filter->date('user.created_at','加入时间')->datetimerange()->width(12);
        $filter->equal('user.phone','手机号码')->width(8);
        return $filter;
    }


    /**
     * @param Table $table
     * @return Table
     * @throws \Exception
     */
    protected function table(Table $table)
    {
        
        $table->column('id', 'ID')->width(80);
        $table->column('user.username', '用户名');
        $table->column('user.nickname', '昵称');
        $table->column('user.avatar', '头像')->image(40);
//        $table->column('gender', '性别');
        $table->column('user.phone', '手机');
        $table->column('user.email', '邮箱');
        $table->column('integral', '积分');
        $table->column('balance', '余额');
        $table->column('user.created_at', '加入时间')->width(110);
        $table->column('user.login_at', '登录时间')->width(110);
        $table->column('user.last_login_ip_at', '登录ip');
//        $table->column('contact_way', '联系方式');
//        $table->column('remark', '备注');
//        $table->column('motto', '个性签名');
//        $table->column('parent_id', '上级id');
//        $table->column('temp_parent_id', '临时上级');
//        $table->column('platform_openid', '平台id 如微信 openid');

        return $table;
    }



    /**
     * 定义form
     * @param Form $form
     * @param Request $request
     * @return Form
     * @throws \think\Exception
     */
    protected function form(Form $form, Request $request)
    {
        
        $form->text('gender', '性别')->rules('require');
//        $form->inputNumber('integral', '积分')->rules('require');
//        $form->inputNumber('total_integral', '最高积分')->rules('require');
//        $form->text('balance', '余额')->number(0, 9999999, 0.01)->rules('require');
//        $form->text('total_balance', '总余额')->number(0, 9999999, 0.01)->rules('require');
        $form->text('contact_way', '联系方式')->rules('require|max:255');
        $form->text('remark', '备注')->rules('require|max:255');
        $form->text('motto', '个性签名')->rules('max:255');
//        $form->inputNumber('parent_id', '上级id')->rules('require');
//        $form->inputNumber('temp_parent_id', '临时上级')->rules('require');
//        $form->text('platform_openid', '平台id 如微信 openid')->rules('require|max:255');

        return $form;
    }



    /**
     * 注册行操作
     * @return array|mixed
     */
    protected function actions()
    {
        return [
            UserInfoIntegralAction::make('积分'),
            UserInfoBalanceAction::make('余额'),
        ];
    }


    /**
     * 注册批量操作
     * @return array
     */
    protected function batchActions()
    {
        return [];
    }


    /**
     * 设置删除
     * @param $action
     * @param $request
     * @return mixed
     */
    protected function deleteAction($action, $request)
    {
        return $action;
    }

    /**
     * 设置编辑
     * @param $action
     * @param $request
     * @return mixed
     */
    protected function editAction($action, $request)
    {
        return $action;
    }


    /**
     *  设置添加
     * @param \quick\admin\actions\Action $action
     * @param Request $request
     * @return \quick\admin\actions\Action
     */
    protected function addAction($action, $request)
    {
        return $action;
    }


}
