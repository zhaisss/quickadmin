<?php

namespace app\admin\quick\actions;


use app\admin\jobs\UpgradeJob;
use quick\admin\actions\Action;
use quick\admin\annotation\AdminAuth;
use quick\admin\components\Component;
use quick\admin\library\queue\Progress;
use quick\admin\library\service\QueueService;

/**
 * 更新系统
 * @AdminAuth(title="更新系统",auth=true,menu=true,login=true)
 * @package app\admin\resource\example\actions
 */
class UpgradeQueueAction extends Action
{


    protected function initAction()
    {

        $this->name = "更新系统";
        $this->confirm('确定更新系统吗？');
    }

    public function load()
    {
        $data = Progress::instance()->setCode(app()->request->param('_keyValues_'))->progress();
        return $this->response()->success('success',$data );
    }

    public function store()
    {
        try {

            saveDataAuth();
            $queue = QueueService::instance()->register('系统更新',UpgradeJob::class);
            $res = Component::custom('queue-log')->props('url',$this->createUrl('load?_keyValues_='.$queue->getCode()));
            $response = $this->response()->success('success',[])->modal($res,'更新系统');
        } catch (\Exception $exception) {
            $response = $this->response()->error($exception->getMessage(),[
                'file' => $exception->getFile(),
                'trace' => $exception->getTrace(),
                'line' => $exception->getLine(),
            ]);
        }
        return $response;
    }


}
