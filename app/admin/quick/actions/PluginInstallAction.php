<?php

namespace app\admin\quick\actions;


use quick\admin\actions\RowAction;
use quick\admin\annotation\AdminAuth;
use quick\admin\components\Component;
use quick\admin\form\Form;
use quick\admin\form\layout\Row;
use quick\admin\library\cloud\CloudException;
use quick\admin\library\cloud\CloudNoLoginException;
use quick\admin\library\service\PluginService;
use quick\admin\library\service\QueueService;
use think\Request;

/**
 * 安装插件
 * @AdminAuth(title="安装插件",auth=true,menu=true,login=true)
 * @package app\admin\resource\example\actions
 */
class PluginInstallAction extends RowAction
{

    /**
     * 模型主键
     *
     * @var string
     */
    public static $pk = "name";

    /**
     * 关联模型
     *
     * @var string
     */
    protected static $model = 'app\common\model\SystemPlugin';


    protected function initAction()
    {

        if(!empty($this->data['status'])){
            $this->param([
                'id' => $this->data['id']
            ]);
        }
        $this->name = "安装";
        if (isset($this->data['addonVersion']) && count($this->data['addonVersion']) > 1) {

            $options = [];
            foreach ($this->data['addonVersion'] as $opt) {
                $options[] = [
                    'name' => $opt['version'],
                    'value' => $opt['version'],
                ];
            }
            $this->display(Component::custom('quick-dropdown')->props([
                'split-button' => false,
                'field' => 'version',
                'title' => $this->name,
                'options' => $options,
            ]));
        } else {

            $this->display(Component::button($this->name())->type('text'));

        }
        $this->confirm('确定安装');

    }


    /**
     * 动作提交数据接口
     *
     * @return mixed
     * @throws \quick\admin\Exception
     * @throws \think\Exception
     */
    public function store()
    {
        $model = $this->findModel();

        return $this->handle($model, $this->request);
    }


    /**
     * 动作异步数据接口
     * @return mixed
     * @throws \quick\admin\Exception
     * @throws \think\Exception
     */
    public function load()
    {
        $model = $this->findModel();

        return $this->resolve($this->request,$model);
    }


    public function handle($model,Request $request)
    {

        try {

            PluginService::instance()->install($this->getPkValue(),false,[
                'id' => $this->request->param('id/d'),
                'version' => $this->request->param('version/s'),
            ]);
            $response = $this->response()->success('success')->message('安装成功')
                ->event('admin_menu',[],200,true);

        } catch (CloudNoLoginException $e) {

            if($e->raw && !empty($e->raw['data']['display'])){
                $form = $e->raw['data']['display'];
//                $form['props']['submitUrl'] = $this->storeUrl(['type' => 'login']);
            }
            $response = $this->response()->success('success', $e->raw)->modal(Component::card()->children([$form]));

        } catch (CloudException $e) {

            return  $this->response()->error($e->getMessage());

        } catch (\Exception $e) {
            $response = $this->response()->error($e->getMessage() ,[
                'line' => $e->getLine(),
                'file' => $e->getFile(),
            ]);
        }
        return $response;
    }


}
