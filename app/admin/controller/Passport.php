<?php
declare(strict_types=1);

namespace app\admin\controller;


use app\common\controller\Backend;
use quick\admin\annotation\AdminAuth;
use quick\admin\http\response\JsonResponse;
use quick\admin\library\service\AuthService;
use quick\admin\library\service\NodeService;
use quick\admin\Quick;
use think\facade\Request;
use think\facade\Validate;

/**
 * @AdminAuth(auth=false,menu=true,login=true,title="登录系统")
 * @package app\admin\controller
 */
class Passport extends Backend
{


    /**
     * @AdminAuth(auth=false,menu=false,login=false,title="登录",log=false)
     * @return \think\response\Json
     * @throws \Exception
     */
    public function login()
    {

        $loginCaptcha = $this->app->session->get("LoginCaptcha", false);
        $rules = [
            'account|账号' => 'require|max:25',
            'password|密码' => 'require|min:6',
        ];

        $data = Request::param();
        if ($loginCaptcha) {
            if(empty($data['captcha']) && !captcha_check($data['captcha'])){
                return JsonResponse::make()->error('验证码有误')->send();
            };
        }


        $validate = Validate::rule($rules);
        if (!$validate->check($data)) {
            return JsonResponse::make()->error($validate->getError())->send();
        }

        $auth = Quick::getAuthService();
        try {
            $res = $auth->login($data['account'], $data['password'], 8 * 60 * 60);
            if ($res) {
                $oplog = [
                    'node' => NodeService::instance()->getCurrent(),
                    'action' => "登录系统",
                    'content' => $this->app->request->url(),
                    'user_agent' =>  $this->app->request->header('user-agent'),
                    'user_id' => $auth->getUserInfo()['user_id'],
                    'geoip' => $this->app->request->ip() ?: '127.0.0.1',
                    'username' => $auth->getUsername() ?: '-',
                    'params' => json_encode([]),
                    'json_result' => json_encode([]),
                ];
                $this->app->db->name('SystemOplog')->insert($oplog) !== false;
                $this->app->session->delete("LoginCaptcha");
                return JsonResponse::make()->success('登录成功')->send();
            }

            $this->app->session->set("LoginCaptcha", true);
            return JsonResponse::make()->error($auth->getError())->send();

        }catch (\Exception $e){
            return JsonResponse::make()->error('登录失败'.$e->getMessage())->send();
        }





    }


    /**
     * @AdminAuth(auth=false,menu=false,login=true,title="员工数据")
     * @return \think\response\Json
     * @throws \Exception
     */
    public function userInfo()
    {
        $auth = Quick::getAuthService();
        if ($auth->isLogin()) {

            $user = $auth->getUserInfo();
            $user['username'] = $user['name'];
            return json(["data" => $auth->getUserInfo(), "code" => 0]);
        }
        return json(["msg" => '请先登录', "code" => 419]);
    }


}
