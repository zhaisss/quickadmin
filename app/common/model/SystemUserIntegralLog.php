<?php
declare (strict_types = 1);

namespace app\common\model;

use quick\admin\http\model\Model;

/**
 * Class SystemUserIntegralLog
 *
 * @property integer  $id              
 * @property integer  $user_id         
 * @property integer  $type            类型:1=收入,2=支出
 * @property integer  $integral        变动积分
 * @property integer  $current_integral 当前积分-变动后
 * @property string   $desc            变动说明
 * @property string   $full_desc       自定义详细说明|记录
 * @property string   $order_no        订单号
 * @property string   $sign            关联订单标识
 * @property string   $created_at      创建时间
 *
 * @package plugins\common\model
 */
class SystemUserIntegralLog extends Model
{


    /**
     * 验证规则
     * @return array
     */
    protected function rules(): array
    {
        return [
            'id' => 'integer',
            'user_id' => 'require|integer',
            'type' => 'require|integer',
            'integral' => 'require|integer',
            'current_integral' => 'require|integer',
            'desc' => 'require|max:255',
            'full_desc' => 'require',
            'sign' => 'require|max:155',
            'order_no' => 'require|max:255',
        ];
    }

  
    /**
     * @return array
     */
    protected function attrLabels(): array
    {
        return [
            'id' => 'id',
            'user_id' => 'user_id',
            'type' => '类型',
            'integral' => '变动积分',
            'current_integral' => '当前积分-变动后',
            'desc' => '变动说明',
            'full_desc' => '自定义详细说明|记录',
            'order_no' => '订单号',
            'sign' => '关联订单标识',
            'created_at' => '创建时间',
        ];
    }


    /**
     * 类型
     */
    public static function getTypeList():array
    {
        return [
            1 => __('Type 1'),
            2 => __('Type 2'),
        ];
    }



    public function user()
    {
        return $this->belongsTo(SystemUser::class,'user_id');
    }
}
