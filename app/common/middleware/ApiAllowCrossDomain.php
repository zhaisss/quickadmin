<?php


declare (strict_types=1);

namespace app\common\middleware;

use Closure;
use think\Request;
use think\Response;
use think\facade\Config;

/**
 * 跨域请求
 */
class ApiAllowCrossDomain
{
    protected $header = [
        'Access-Control-Allow-Credentials' => 'true',
        'Access-Control-Max-Age'           => 1800,
        'Access-Control-Allow-Methods'     => 'GET, POST, PATCH, PUT, DELETE, OPTIONS,FETCH',
        'Access-Control-Allow-Headers'     => '*',
    ];

    /**
     * 跨域请求检测
     * @access public
     * @param Request $request
     * @param Closure $next
     * @param array   $header
     * @return Response
     */
    public function handle($request, Closure $next, ?array $header = [])
    {
        $header = !empty($header) ? array_merge($this->header, $header) : $this->header;


        $origin = $request->header('origin');
        if ($origin) {
            $info = parse_url($origin);
            // 获取跨域配置
            $corsDomain   = explode(',', "*");
            $corsDomain[] = $request->host(true);

            if (in_array("*", $corsDomain) || in_array($origin, $corsDomain) || (isset($info['host']) && in_array($info['host'], $corsDomain))) {
                header("Access-Control-Allow-Origin: " . $origin);
            }
        }

        return $next($request)->header($header);
    }
}
