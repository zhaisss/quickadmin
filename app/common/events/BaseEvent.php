<?php


namespace app\common\events;


use app\common\service\CommonService;

class BaseEvent extends CommonService
{


    /**
     * @var Object 事件发起人对象
     */
    public $sender;


    /**
     * @param ...$arguments
     * @return static
     */
    public static function dispatch(...$arguments)
    {
        $obj = new static(...$arguments);
        event($obj);
        return $obj;
    }


    /**
     * @return mixed
     */
    public function event()
    {
        return event($this);
    }
}