<?php
declare(strict_types=1);

namespace components\admin\src\auth;


use quick\admin\actions\BatchDeleteAction;
use quick\admin\actions\ViewAction;
use quick\admin\annotation\AdminAuth;
use quick\admin\components\Component;
use quick\admin\filter\Filter;
use quick\admin\form\Form;
use quick\admin\Resource;
use quick\admin\table\Table;
use think\Request;

/**
 * 系统日志
 * @AdminAuth(title="系统日志",auth=true,menu=true,login=true)
 * @package app\admin\resource\auth
 */
class Oplog extends Resource
{
    /**
     * @var string
     */
    protected $title = '系统日志';

    /**
     * @var string
     */
    protected $description = "系统日志列表";

    /**
     * @var string
     */
    protected static $model = "app\\common\\model\\SystemOplog";


    /**
     * 可搜索字段
     *
     * @var array
     */
    protected static $search = [
        'username','geoip','node'
    ];




    protected function model($model)
    {
        return $model->order("id desc");
    }

    /**
     * 过滤器
     * @param Request $request
     * @return array
     */
    protected function filters(Filter $filter)
    {
        $filter->equal("username", "操作人")->width(8);
        $filter->date("created_at", "操作时间")->datetimerange()->width(12);

        return $filter;
    }

    /**
     * @param Table $table
     * @return Table
     * @throws \Exception
     */
    protected function table(Table $table)
    {

        $table->paginate(10);
        $table->dataUsing(function ($data) {
            $ip = new \Ip2Region();
            $data = $data->toArray();
            foreach ($data as &$vo) {
                $isp = $ip->btreeSearch($vo['geoip']);
                $vo['isp'] = str_replace(['内网IP', '0', '|'], '', $isp['region'] ?? '');
            }
            return $data;
        });
        $table->props('border', false);
        $table->column("id", "ID")->width(80);
        $table->column("username", "操作人")->width(100);
        $table->column("action", "操作")->width(190);
//        $table->column("plugin", "系统模块")->width(80);
//        $table->column("method", "请求方式")->width(80);
        $table->column("node", "操作权限");
//        $table->column("content", "url");
        $table->column("geoip", "地理位置")->width(120)->display(function ($value, $row, $v) {

            return Component::html($row['geoip'] . '<br>' . $row['isp']);
        });
        $table->column("created_at", "操作时间")->width(170);
        return $table;
    }


    protected function form(Form $form, Request $request)
    {

//        $form->radio('type', '日志类型')->disabled()->options([0=>'后台管理',1=>'用户接口']);
        $form->text('plugin', '系统模块')->disabled();
        $form->text('username', '操作人')->disabled();
        $form->text('user_id', '账户id')->disabled();
        $form->text('action', '操作');
        $form->text('node', '权限节点');
        $form->text('method', '请求方法');
        $form->text('geoip', 'IP');
        $form->text('user_agent', 'User-Agent');
        $form->text('content', 'URL');
        $form->text('params', '请求参数')->textarea();
        $form->text('json_result', '反回数据')->textarea();
        $form->text('error_msg', '错误信息');
        return $form;
    }


    /**
     * 注册行操作
     * @return array|mixed
     */
    protected function actions()
    {
        return [
            ViewAction::make('查看')->setForm($this->getForm())
        ];
    }


    /**
     * 注册批量操作
     * @return array
     */
    protected function batchActions()
    {
        return [
            BatchDeleteAction::make("删除")
        ];
    }


    /**
     * @param $action
     * @param $request
     * @return mixed
     */
    protected function deleteAction($action, $request)
    {
        return false;
    }

    /**
     * @param \quick\actions\RowAction|\quick\admin\actions\Action $action
     * @param Request $request
     * @return bool|\quick\actions\RowAction
     */
    protected function editAction($action, $request)
    {
        return false;
    }


    /**
     * @param \quick\admin\actions\Action $action
     * @param Request $request
     * @return bool|\quick\admin\actions\Action
     */
    protected function addAction($action, $request)
    {
        return false;
    }


}
