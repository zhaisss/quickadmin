<!--本项目前端layout模块基于 Fantastic-admin 修改定制-->
<!-- layout模块版权归  Fantastic-admin  -->
<!-- 项目地址：https://gitee.com/hooray/fantastic-admin  -->

# vue3_vite_quick3

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn serve
```

### Compiles and minifies for production
```
yarn build
```

### Lints and fixes files
```
yarn lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
